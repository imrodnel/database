-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 25 Mars 2015 à 15:22
-- Version du serveur: 5.5.41-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `MASTERU-TENNIS`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `AdminInsertPlayer`(IN `Tea` VARCHAR(20), IN `Na` VARCHAR(20), IN `Fir` VARCHAR(20), IN `Se` ENUM('M','W'), IN `DateB` DATE)
    NO SQL
BEGIN
DECLARE ret int;
IF ( (SELECT COUNT(*) FROM Players 
    WHERE Team=Tea
    AND Name=Na
    AND FirstName=Fir
    AND Sex=Se
    AND DateBirth=DateB
   ) = 0 )
THEN
	INSERT INTO `Players`(`Team`, `Name`, `FirstName`, `Sex`, `DateBirth`) VALUES (Tea,Na,Fir,Se,DateB);
	select IdPlayer from Players where Team=Tea
    	AND Name=Na
    	AND FirstName=Fir
    	AND Sex=Se
    	AND DateBirth=DateB;
	
ELSE 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PLAYER ALREADY PRESENT';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AdminUpdatePlayer`(IN `Id` INT, IN `Tea` VARCHAR(20), IN `No` VARCHAR(20), IN `Fir` VARCHAR(20), IN `Se` ENUM('M','W'), IN `DateB` DATE)
    NO SQL
BEGIN
	UPDATE `Players` SET `Team`=Tea,`Name`=No,`FirstName`=Fir,`Sex`=Se,`DateBirth`=DateB WHERE `IdPlayer`=Id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AdminUpdateScore`(IN `idM` INT, IN `Rf` ENUM('A','B'), IN `Set1A` INT, IN `Set1B` INT, IN `Set2A` INT, IN `Set2B` INT, IN `Set3A` INT, IN `Set3B` INT, IN `Set4A` INT, IN `Set4B` INT, IN `Set5A` INT, IN `Set5B` INT)
    NO SQL
BEGIN
DECLARE stat VARCHAR(10);
SELECT `Statut` INTO stat FROM `Matchs` WHERE `IdMatch` = idM;

IF stat = 'PROGRESS' THEN 
	IF Set1A<>null AND Set1A<>null THEN 
		UPDATE `DisplayScores` SET `Set1`=Set1A WHERE `IdMatch`=idM AND `Ref`=A;
		UPDATE `DisplayScores` SET `Set1`=Set1B WHERE `IdMatch`=idM AND `Ref`=B;
	END IF;
	IF Set2A<>null AND Set2B<>null THEN 
		UPDATE `DisplayScores` SET `Set2`=Set2A WHERE `IdMatch`=idM AND `Ref`=A;
		UPDATE `DisplayScores` SET `Set2`=Set2B WHERE `IdMatch`=idM AND `Ref`=B;
	END IF;
	IF Set3A<>null AND Set3B<>null THEN 
		UPDATE `DisplayScores` SET `Set3`=Set3A WHERE `IdMatch`=idM AND `Ref`=A;
		UPDATE `DisplayScores` SET `Set3`=Set3B WHERE `IdMatch`=idM AND `Ref`=B;
	END IF;
	IF Set4A<>null AND Set4B<>null THEN 
		UPDATE `DisplayScores` SET `Set4`=Set4A WHERE `IdMatch`=idM AND `Ref`=A;
		UPDATE `DisplayScores` SET `Set4`=Set4B WHERE `IdMatch`=idM AND `Ref`=B;
	END IF;
	IF Set5A<>null AND Set5B<>null THEN 
		UPDATE `DisplayScores` SET `Set5`=Set5A WHERE `IdMatch`=idM AND `Ref`=A;
		UPDATE `DisplayScores` SET `Set5`=Set5B WHERE `IdMatch`=idM AND `Ref`=B;
	END IF;
	CALL EndMatch(IdM,Rf,GETDATE());
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NOT IN PROGRESS';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AffectPlayerMatch`(IN `IdM` INT, IN `IdPA_OoT1` INT, IN `IdPB_OoT1` INT, IN `IdPA_OoT2` INT, IN `IdPB_OoT2` INT)
    NO SQL
BEGIN
declare IdOoT1 int;
declare IdOoT2 int;
SELECT `IdOneOrTwo1`INTO IdOoT1 FROM `Matchs` WHERE `IdMatch`=IdM;
SELECT `IdOneOrTwo2`INTO IdOoT2 FROM `Matchs` WHERE `IdMatch`=IdM;
call AffectPlayerOneOrTwo(IdOoT1,IdPA_OoT1,IdPB_OoT1);
call AffectPlayerOneOrTwo(IdOoT2,IdPA_OoT2,IdPB_OoT2);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `AffectPlayerOneOrTwo`(IN `IdOoT` INT, IN `IdPA` INT, IN `IdPB` INT)
    NO SQL
BEGIN
UPDATE `OneOrTwo` SET `IdPlayer_A`=IdPA,`IdPlayer_B`=IdPB WHERE `IdOneOrTwo`=IdOoT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CancelMatch`(IN `IdMa` INT)
    MODIFIES SQL DATA
BEGIN
DECLARE sc1, sc2,nbMa,IdHug int;
DECLARE stat varchar(20);

SELECT  `Statut` into stat FROM `Matchs` WHERE `IdMatch`=IdMa;

IF stat <> 'PROGRESS' THEN
	
	UPDATE `Matchs` SET `Statut`='CANCEL' WHERE `IdMatch`=IdMa;
	SELECT `IdHugeMatch` INTO IdHug FROM `HugeMatch` WHERE  `IdMatch`=IdMa;
	SET nbMa=NbrMatch(IdHug);
	SELECT SUM(Score_A), SUM(Score_B) INTO sc1, sc2 FROM HugeMatch WHERE IdHugeMatch = IdHug;
			
	IF (sc1 + sc2 >= nbMa) THEN
		UPDATE `HugeMatch` SET `HugeStatut` = 'FINISH' WHERE IdHugeMatch = IdHug; 	
	END IF;

ELSE

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH IN PROGRESS';

END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateTournament`(IN `namet` INT)
    MODIFIES SQL DATA
UPDATE Tournament SET `Name` = namet, `Stage` = 'SETTING_UP'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteOneOrTwo`(IN `id` INT)
    MODIFIES SQL DATA
BEGIN

IF ((SELECT Stage FROM Tournamaent) = 'SETTING-UP') THEN

	IF ((SELECT COUNT(*) FROM OneOrTwo WHERE IdOneOrTwo = id) > 0) THEN

		IF ((SELECT COUNT(*) FROM Matchs WHERE IdOneOrTwo1 = id AND Statut = 'PROGRESS') > 0
	       OR (SELECT COUNT(*) FROM Matchs WHERE IdOneOrTwo2 = id AND Statut = 'PROGRESS') > 0 ) THEN
		
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ID ONEORTWO IS USED IN A CURRENT MATCH '; 

		ELSE

			UPDATE OneOrTwo SET IdPlayer_A = NULL, IdPlayer_B = NULL WHERE IdOneOrTwo = id;

		END IF;

	ELSE
	
		SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'ID ONEORTWO DOES NOT EXIST'; 
   	
	END IF;

ELSE

		SIGNAL SQLSTATE '45002' SET MESSAGE_TEXT = 'TOURNAMENT IN PROGRESS'; 		
	   	
END IF;	   	

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DeletePlayer`(IN `idplay` INT)
BEGIN


IF ((SELECT Stage FROM Tournament) = 'SETTING-UP') THEN

	IF ((SELECT COUNT(*) FROM Players WHERE IdPlayer = idplay) > 0) THEN
	
			DELETE FROM Players WHERE IdPlayer = idplay;
	
	ELSE

			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PLAYER DOES NOT EXIST'; 
	
	END IF;

ELSE
   			SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'TOURNAMENT IN PROGRESS'; 		

END IF;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteTeam`(IN `id` INT(10))
    MODIFIES SQL DATA
BEGIN

DECLARE stag VARCHAR(50);
DECLARE nam VARCHAR(4);

SELECT Stage INTO stag FROM Tournament;

IF (stag = 'SETTING-UP') THEN

	CASE id 
		
		WHEN 1 THEN
	
			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe1', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;

		WHEN 2 THEN	
		
			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe2', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
					
		WHEN 3 THEN
			
			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe3', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
			
		WHEN 4 THEN	
		
			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe4', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
			
		WHEN 5 THEN

			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe5', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
						
		WHEN 6 THEN

			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe6', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
						
		WHEN 7 THEN
			
			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe7', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
						
		WHEN 8 THEN	
		
			SELECT NameTeam INTO nam FROM Teams WHERE IdTeam = id;
			DELETE FROM Players WHERE Team = nam;
			UPDATE Teams SET NameTeam = 'Equipe8', CompleteName = NULL, Points = 0, URL = NULL WHERE IdTeam = id;
						
	END CASE;
	
ELSE
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TOURNAMENT IN PROGRESS'; 		
	   	
END IF;	   	


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EndMatch`(IN `idm` INT, IN `Win` ENUM('A','B'), IN `DatEnd` DATETIME)
    COMMENT 'Procedure de fin de match..'
BEGIN

DECLARE stat VARCHAR(10);
DECLARE IdHug VARCHAR(10);
DECLARE c INT;

SELECT Court INTO c FROM `Matchs` WHERE IdMatch = idm;

SELECT `Statut`, IdHugeMatch INTO stat, IdHug FROM `Matchs` WHERE `IdMatch` = idm;

	IF stat = 'PROGRESS' THEN
		UPDATE `Matchs` SET `Statut`= 'FINISH',`Winner`= Win, `DateEnd` = DatEnd WHERE `IdMatch` = idm;
		CALL UpdHugeMatch (IdHug, idm, Win);
		CALL UpdateStatMatch(idm);
	ELSE

		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NOT STARTED'; 
	
	END IF;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetCourt`()
    NO SQL
BEGIN

CREATE TEMPORARY TABLE IF NOT EXISTS 
  temp2 (Court int);
 
IF (SELECT COUNT(*) FROM temp2) = 0 THEN
	INSERT INTO temp2 VALUES(1);
	INSERT INTO temp2 VALUES(2);
	INSERT INTO temp2 VALUES(3);
	INSERT INTO temp2 VALUES(4);
	INSERT INTO temp2 VALUES(5);
	INSERT INTO temp2 VALUES(6);
	INSERT INTO temp2 VALUES(7);
END IF;

SELECT Court FROM temp2 WHERE temp2.Court NOT IN 
	(SELECT Court FROM Matchs WHERE Statut = 'PROGRESS');

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetMatchScore`(IN `idm` INT(50))
    NO SQL
BEGIN

DECLARE id1 INT(50);
DECLARE id2 INT(50);

IF ((SELECT COUNT(*) FROM Matchs WHERE IdMatch = idm) = 1) THEN

	SELECT IdOneOrTwo1, IdOneOrTwo2 INTO id1, id2  FROM Matchs WHERE IdMatch = idm;

	SELECT IdMatch, (SELECT Set1 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id1) AS SetA1, 
		(SELECT Set2 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id1) AS SetA2,
		(SELECT Set3 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id1) AS SetA3,
		(SELECT Set4 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id1) AS SetA4, 
		(SELECT Set5 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id1) AS SetA5,
		(SELECT Score FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id1) AS Score_A,
		
		(SELECT Set1 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id2) AS SetB1,
		(SELECT Set2 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id2) AS SetB2,
		(SELECT Set3 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id2) AS SetB3, 
		(SELECT Set4 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id2) AS SetB4, 
		(SELECT Set5 FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id2) AS SetB5,
		(SELECT Score FROM DisplayScores WHERE DisplayScores.IdMatch = idm AND IdOneOrTwo = id2) AS Score_B
	FROM DisplayScores WHERE IdMatch = idm GROUP BY IdMatch;

ELSE

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'IDMATCH DOES NOT EXIST'; 

END IF;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetPhase`(IN `nam` VARCHAR(50))
    READS SQL DATA
SELECT `Stage` FROM Tournament WHERE Name = nam$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetStatPlayer`(IN `IdJ` INT)
    NO SQL
SELECT 
	(SELECT IdPlayerToName(IdJ)) AS Name,
	`IdPlayer`,
	(SELECT SUM(`ACE`)) AS `ACE`,
	(SELECT SUM(`WS`)) AS `WS` ,
	(SELECT SUM( `WP`)) AS `WP`,
	(SELECT SUM( `DF`)) AS `DF`,
	(SELECT SUM( `UE`)) AS `UE`,
	(SELECT SUM( `FE`)) AS `FE`,
	(SELECT SUM( `FSF`)) AS `FSF`,
	(SELECT SUM( `Break`)) AS `Break`
FROM `StatMatch` WHERE `IdPlayer` =IdJ$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetStatPlayerMatch`(IN `IdJ` INT, IN `IdM` INT)
    NO SQL
BEGIN
IF (SELECT COUNT(*) FROM `StatMatch` WHERE `IdMatch`=IdM AND `IdPlayer`=IdJ)=1 THEN 
	SELECT 
		(SELECT IdPlayerToName(IdJ)) AS Name,
		`IdPlayer`,
		`IdMatch`,
		(SELECT SUM(`ACE`)) AS `ACE`,
		(SELECT SUM(`WS`)) AS `WS` ,
		(SELECT SUM( `WP`)) AS `WP`,
		(SELECT SUM( `DF`)) AS `DF`,
		(SELECT SUM( `UE`)) AS `UE`,
		(SELECT SUM( `FE`)) AS `FE`,
		(SELECT SUM( `FSF`)) AS `FSF`,
		(SELECT SUM( `Break`)) AS `Break`
	FROM `StatMatch` WHERE `IdPlayer` =IdJ AND `IdMatch`= IdM;
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'IDMATCH AND IDPLAYER NOT COMPATIBLE';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetStatsAce`(IN `IdJ` INT, IN `IdM` INT)
    NO SQL
BEGIN

	DECLARE Re VARCHAR (2);
	
	SELECT IdPlayerToRef (IdJ,IdM) INTO Re;

	SELECT COUNT(*) FROM Log WHERE IdMatch = IdM AND Ref = Re AND Statistic = 'ACE';


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetTirage`()
    NO SQL
SELECT `Team_A`,`Team_B` FROM `HugeMatch` WHERE `HugeStatut` = 'PROGRESS' GROUP BY `IdHugeMatch`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetTournament`()
    READS SQL DATA
SELECT `Name`, `Stage` FROM Tournament$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `GetUrlTeam`(IN `Tea` VARCHAR(20))
    NO SQL
Begin
IF (SELECT COUNT(*) FROM Teams WHERE `NameTeam`=Tea )=1 THEN
	SELECT `URL` FROM `Teams` WHERE `NameTeam`=Tea;
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TEAM NOT PRESENT';
END IF;
End$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `IdMatchtoIdPoint`(IN `Idm` INT)
    NO SQL
BEGIN
IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	select max(Id) as maxPointId from `Log` where IdMatch=idm;
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NOT CREATED';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfCou`(IN `numcourt` INT)
    NO SQL
BEGIN

DECLARE idm int;	
DECLARE type char(5);
DECLARE IdOoT1 int;
DECLARE IdOoT2 int;
DECLARE IdHug int;

IF (numcourt = (SELECT Court FROM Matchs WHERE (`Statut` = 'PROGRESS' AND Court = numcourt))) THEN
	
	SELECT `Category` INTO type FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';
	
	CASE type

		WHEN 'DM' THEN
			SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE (`Statut` = 'PROGRESS' AND Court = numcourt);
			CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);	
		WHEN 'DW' THEN
			SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE (`Statut` = 'PROGRESS' AND Court = numcourt);
			CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);				
		WHEN 'DX' THEN
			SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE (`Statut` = 'PROGRESS' AND Court = numcourt);
			CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);		
    	WHEN 'SM' THEN
			SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE (`Statut` = 'PROGRESS' AND Court = numcourt);
			CALL InfoSimple(idm, IdOoT1, IdOoT2, IdHug);		
    	WHEN 'SW' THEN
			SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE (`Statut` = 'PROGRESS' AND Court = numcourt);
			CALL InfoSimple(idm, IdOoT1, IdOoT2, IdHug);
		
    END CASE;

END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfoBracket`()
    NO SQL
BEGIN

SELECT `IdHugeMatch`, `Team_A`, SUM(`Score_A`) AS Score_A, `Team_B`, SUM(`Score_B`) AS Score_B 
FROM `HugeMatch` 
WHERE   `Team_A` IS NOT NULL AND  `Team_B` IS NOT NULL
GROUP BY `IdHugeMatch`;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfoCourt`(IN `numcourt` INT)
    NO SQL
BEGIN

DECLARE idm int;	
DECLARE type char(5);
DECLARE IdOoT1 int;
DECLARE IdOoT2 int;
DECLARE IdHug int;

IF ((SELECT COUNT(*) FROM Matchs WHERE (`Statut` = 'PROGRESS' AND `Court` = numcourt)) > 0) THEN

SELECT `Category` INTO type FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';

CASE type

	WHEN 'DM' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';
		CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);	
	WHEN 'DW' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';
		CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);				
	WHEN 'DX' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';
		CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);		
   	WHEN 'SM' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';
		CALL InfoSimple(idm, IdOoT1, IdOoT2, IdHug);		
    WHEN 'SW' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'PROGRESS';
		CALL InfoSimple(idm, IdOoT1, IdOoT2, IdHug);
		
END CASE;

ELSEIF ((SELECT COUNT(*) FROM Matchs WHERE (`Statut` = 'FINISH' AND `Court` = numcourt)) > 0) THEN

SELECT `Category` INTO type FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'FINISH' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(DateEnd)) / 60) < 15 order by UNIX_TIMESTAMP(DateEnd) desc limit 1;

Case type
	WHEN 'DM' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'FINISH' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(DateEnd)) / 60) < 15 order by UNIX_TIMESTAMP(DateEnd) desc limit 1;
		CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);	
	WHEN 'DW' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'FINISH' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(DateEnd)) / 60) < 15 order by UNIX_TIMESTAMP(DateEnd) desc limit 1;
		CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);				
	WHEN 'DX' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'FINISH' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(DateEnd)) / 60) < 15 order by UNIX_TIMESTAMP(DateEnd) desc limit 1;
		CALL InfoDouble(idm, IdOoT1, IdOoT2, IdHug);		
   	WHEN 'SM' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'FINISH' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(DateEnd)) / 60) < 15 order by UNIX_TIMESTAMP(DateEnd) desc limit 1;
		CALL InfoSimple(idm, IdOoT1, IdOoT2, IdHug);		
    WHEN 'SW' THEN
		SELECT `IdMatch`,IdHugeMatch, `IdOneOrTwo1`,`IdOneOrTwo2` INTO idm, IdHug, IdOoT1, IdOoT2 FROM `Matchs` WHERE `Court` = numcourt AND Statut = 'FINISH' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(DateEnd)) / 60) < 15 order by UNIX_TIMESTAMP(DateEnd) desc limit 1;
		CALL InfoSimple(idm, IdOoT1, IdOoT2, IdHug);	
END CASE;

ELSEIF ((SELECT COUNT(*) FROM Matchs WHERE (`Court` = numcourt)) = 0) THEN

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'NUMBER OF COURT NOT PRESENT';

END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfoDouble`(IN `idm` INT, IN `IdOoT1` INT, IN `IdOoT2` INT, IN `IdHug` INT)
    NO SQL
    COMMENT 'Procedure permettant d''afficher les SET et SCORES'
BEGIN

DECLARE s2,s3,s4,s5 int;
DECLARE s6,s7,s8,s9 int;
DECLARE idj1 int;
DECLARE idj2 int;
DECLARE idj3 int;
DECLARE idj4 int;

SELECT `Set2`,`Set3`,`Set4`,`Set5` INTO s2,s3,s4,s5 FROM `DisplayScores` WHERE `IdCheck` IN (SELECT MIN(IdCheck) FROM `DisplayScores` WHERE `IdMatch`=idm);
SELECT `Set2`,`Set3`,`Set4`,`Set5` INTO s6,s7,s8,s9 FROM `DisplayScores` WHERE `IdCheck` IN (SELECT MAX(IdCheck) FROM `DisplayScores` WHERE `IdMatch`=idm);
		
IF (s2 < 0 OR s6 < 0) THEN
		SELECT IdPlayer_A, IdPlayer_B  INTO idj1, idj2 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A, IdPlayer_B INTO idj3, idj4 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj1, idj2) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm) GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj3, idj4) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;

ELSEIF (s3 < 0 OR s7 < 0) THEN
		SELECT IdPlayer_A, IdPlayer_B  INTO idj1, idj2 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A, IdPlayer_B  INTO idj3, idj4 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;


		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj1, idj2) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj3, idj4) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;
   
ELSEIF (s4 < 0 OR s8 < 0) THEN
		SELECT IdPlayer_A, IdPlayer_B  INTO idj1, idj2 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A, IdPlayer_B  INTO idj3, idj4 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj1, idj2) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj3, idj4) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;
            
ELSEIF (s5 < 0 OR s9 < 0) THEN    
		SELECT IdPlayer_A, IdPlayer_B  INTO idj1, idj2 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A, IdPlayer_B  INTO idj3, idj4 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;


		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj1, idj2) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj3, idj4) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;           
            
ELSE
		SELECT IdPlayer_A, IdPlayer_B  INTO idj1, idj2 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A, IdPlayer_B  INTO idj3, idj4 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;


		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Set5, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj1, idj2) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Set5, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND Players.IdPlayer IN (idj3, idj4) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;     	     	
        
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfoMatch`(IN `Idm` INT)
    NO SQL
BEGIN
DECLARE cat VARCHAR(3);


IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	SELECT `Category` INTO cat FROM `Matchs` WHERE `IDMatch`=Idm;
	IF cat = 'SM' OR cat = 'SW' THEN
		SELECT 
			Team_A,
			(SELECT IdPlayer FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A) AND IdOneOrTwo = IdOneOrTwo1) AS 
			IdPlayerA,
			(SELECT (CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A) AND IdOneOrTwo = IdOneOrTwo1) AS 
			Name_CompletA, 
			Team_B, 
			(SELECT IdPlayer FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A) AND IdOneOrTwo = IdOneOrTwo2) AS 
			IdPlayerB,
			(SELECT (CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A) AND IdOneOrTwo = IdOneOrTwo2) AS 
			Name_CompletB, 
			Category, 
			Tableau,
			Statut,
			DateStart,
			DateEnd 
		FROM Matchs, HugeMatch, Players
		WHERE Matchs.IdMatch=Idm GROUP BY Matchs.IdMatch;
	ELSE
		SELECT 
			Team_A,
			(SELECT IdPlayer FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A) AND IdOneOrTwo = IdOneOrTwo1) AS 
			IdPlayerA_1,
			(SELECT IdPlayer FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1) AS 
			IdPlayerA_2,
			(SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1) AS 
			Name_CompletA, 
			Team_B, 
			(SELECT IdPlayer FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A) AND IdOneOrTwo = IdOneOrTwo2) AS 
			IdPlayerB_1,
			(SELECT IdPlayer FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2) AS 
			IdPlayerB_2,
			(SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2) AS 
			Name_CompletB, 
			Category, 
			Tableau,
			Statut,
			DateStart,
			DateEnd 
		FROM Matchs, HugeMatch, Players
		WHERE Matchs.IdMatch=Idm GROUP BY Matchs.IdMatch;
	END IF;
ELSE 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NON CREE';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfosHugeMatchs`(IN `tab` ENUM('1/4','1/2','Final'))
    NO SQL
BEGIN

CASE tab

WHEN '1/4' THEN

SELECT Matchs.IdHugeMatch, Matchs.IdMatch, Statut, Matchs.Category, Tableau, Court, Team_A AS Pays_A, (SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1) AS Players_A,
		(SELECT Set1 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA1, 
		(SELECT Set2 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA2,
		(SELECT Set3 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA3,
		(SELECT Set4 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA4, 
		(SELECT Set5 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA5,
		(SELECT Score FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS Score_A,
		
		Team_B AS Pays_B, (SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2) AS Players_B,
		(SELECT Set1 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB1,
		(SELECT Set2 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB2,
		(SELECT Set3 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB3, 
		(SELECT Set4 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB4, 
		(SELECT Set5 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB5,
		(SELECT Score FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS Score_B,
		(SELECT Service FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS ServiceA,
		(SELECT Service FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS ServiceB,
		Matchs.Winner 
FROM Matchs, HugeMatch
WHERE (Statut = 'PROGRESS' OR Statut = 'FINISH' OR Statut = 'SOON') AND (HugeStatut = 'PROGRESS' OR HugeStatut = 'FINISH' OR HugeStatut = 'SOON') AND HugeMatch.IdHugeMatch = Matchs.IdHugeMatch AND HugeMatch.IdMatch = Matchs.IdMatch AND Tableau = '1/4-P' GROUP BY Matchs.IdMatch;

WHEN '1/2' THEN

SELECT Matchs.IdHugeMatch, Matchs.IdMatch, Statut, Matchs.Category, Tableau, Court, Team_A AS Pays_A, (SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1) AS Players_A,
		(SELECT Set1 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA1, 
		(SELECT Set2 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA2,
		(SELECT Set3 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA3,
		(SELECT Set4 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA4, 
		(SELECT Set5 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA5,
		(SELECT Score FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS Score_A,
	
		Team_B AS Pays_B, (SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2) AS Players_B,
		(SELECT Set1 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB1,
		(SELECT Set2 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB2,
		(SELECT Set3 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB3, 
		(SELECT Set4 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB4, 
		(SELECT Set5 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB5,
		(SELECT Score FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS Score_B,
		(SELECT Service FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS ServiceA,
		(SELECT Service FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS ServiceB,
		Matchs.Winner
FROM Matchs, HugeMatch
WHERE (Statut = 'PROGRESS' OR Statut = 'FINISH' OR Statut = 'SOON') AND (HugeStatut = 'PROGRESS' OR HugeStatut = 'FINISH' OR HugeStatut = 'SOON') AND HugeMatch.IdHugeMatch = Matchs.IdHugeMatch AND HugeMatch.IdMatch = Matchs.IdMatch AND (Tableau = '1/2-P' OR Tableau = '1/2-5') GROUP BY Matchs.IdMatch;


WHEN 'Final' THEN

SELECT Matchs.IdHugeMatch, Matchs.IdMatch, Statut, Matchs.Category, Tableau, Court, Team_A AS Pays_A, (SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1) AS Players_A,
		(SELECT Set1 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA1, 
		(SELECT Set2 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA2,
		(SELECT Set3 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA3,
		(SELECT Set4 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA4, 
		(SELECT Set5 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS SetA5,
		(SELECT Score FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS Score_A,
				
		Team_B AS Pays_B, (SELECT GROUP_CONCAT((CONCAT_WS ('.',(LEFT(FirstName,1)),Name)) SEPARATOR ' / ') FROM Players, OneOrTwo WHERE IdPlayer IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2) AS Players_B,
		(SELECT Set1 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB1,
		(SELECT Set2 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB2,
		(SELECT Set3 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB3, 
		(SELECT Set4 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB4, 
		(SELECT Set5 FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS SetB5,
		(SELECT Score FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS Score_B,
		(SELECT Service FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo1) AS ServiceA,
		(SELECT Service FROM DisplayScores WHERE Matchs.IdMatch = DisplayScores.IdMatch AND IdOneOrTwo = IdOneOrTwo2) AS ServiceB,
		Matchs.Winner 
FROM Matchs, HugeMatch
WHERE (Statut = 'PROGRESS' OR Statut = 'FINISH' OR Statut = 'SOON') AND (HugeStatut = 'PROGRESS' OR HugeStatut = 'FINISH' OR HugeStatut = 'SOON') AND HugeMatch.IdHugeMatch = Matchs.IdHugeMatch AND HugeMatch.IdMatch = Matchs.IdMatch AND (Tableau = 'Final-P' OR Tableau = 'Final-5' OR Tableau = 'Final-3' OR Tableau = 'Final-7') GROUP BY Matchs.IdMatch;

END CASE;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfoSimple`(IN `idm` INT, IN `IdOoT1` INT, IN `IdOoT2` INT, IN `IdHug` INT)
    NO SQL
    COMMENT 'Procedure d''affichage des Players de jeu simple'
BEGIN

DECLARE s2,s3,s4,s5 int;
DECLARE s6,s7,s8,s9 int;
DECLARE idj1 int;
DECLARE idj2 int;
DECLARE idj3 int;
DECLARE idj4 int;

SELECT `Set2`,`Set3`,`Set4`,`Set5` INTO s2,s3,s4,s5 FROM `DisplayScores` WHERE `IdCheck` IN (SELECT MIN(IdCheck) FROM `DisplayScores` WHERE `IdMatch`=idm);
SELECT `Set2`,`Set3`,`Set4`,`Set5` INTO s6,s7,s8,s9 FROM `DisplayScores` WHERE `IdCheck` IN (SELECT MAX(IdCheck) FROM `DisplayScores` WHERE `IdMatch`=idm);
		
IF (s2 < 0 OR s6 < 0) THEN
		SELECT IdPlayer_A INTO idj1 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A INTO idj3 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj1) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm) GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj3) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;

ELSEIF (s3 < 0 OR s7 < 0) THEN
		SELECT IdPlayer_A INTO idj1 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A INTO idj3 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj1) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj3) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;
   
ELSEIF (s4 < 0 OR s8 < 0) THEN
		SELECT IdPlayer_A INTO idj1 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A INTO idj3 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj1) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj3) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;
            
ELSEIF (s5 < 0 OR s9 < 0) THEN    
		SELECT IdPlayer_A INTO idj1 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A INTO idj3 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj1) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj3) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;           
            
ELSE
		SELECT IdPlayer_A INTO idj1 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT1;
		SELECT IdPlayer_A INTO idj3 FROM OneOrTwo WHERE IdOneOrTwo = IdOoT2;

		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Set5, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj1) AND OneOrTwo.IdOneOrTwo = IdOoT1 AND DisplayScores.IdOneOrTwo = IdOoT1 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team 
		UNION ALL
		SELECT     Players.Team, GROUP_CONCAT((CONCAT_WS('.',(LEFT(Players.FirstName,1)),Players.Name)) SEPARATOR ' / ') AS Name, DisplayScores.Set1, DisplayScores.Set2, DisplayScores.Set3, DisplayScores.Set4, DisplayScores.Set5, DisplayScores.Score, DisplayScores.Service, 
                   DisplayScores.Ref, Matchs.Category, Court, Tableau, Winner, TIME_FORMAT((SELECT TimeMatch (idm)), '%H:%i') AS Duree
		FROM       OneOrTwo, Players, Matchs, DisplayScores, HugeMatch
		WHERE     (Matchs.IdMatch = idm AND (Players.IdPlayer = idj3) AND OneOrTwo.IdOneOrTwo = IdOoT2 AND DisplayScores.IdOneOrTwo = IdOoT2 AND Matchs.IdHugeMatch = IdHug AND HugeMatch.IdMatch = idm)  GROUP BY Players.Team;     	     	
        
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfosOfCourt`()
    NO SQL
BEGIN

      DECLARE a INT DEFAULT 0 ;
      SIMPLE_LOOP: LOOP
         SET a = a + 1;
         CALL InfCou (a);
         IF a = 9 THEN
            LEAVE SIMPLE_LOOP;
         END IF;
   END LOOP SIMPLE_LOOP;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfosPlayer`(IN `idj` INT)
    NO SQL
BEGIN


IF ((SELECT COUNT(*) FROM Players WHERE IdPlayer = idj) = 0) THEN

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PLAYER DOES NOT EXIST '; 

ELSE

	SELECT * FROM Players WHERE IdPlayer = idj;

END IF;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfosPlayersOfTeam`(IN `t` VARCHAR(4))
    NO SQL
BEGIN


IF ((SELECT COUNT(*) FROM Teams WHERE NameTeam = t) = 0) THEN

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TEAM DOES NOT EXIST '; 

ELSE

	SELECT * FROM Players WHERE Team = t;

END IF;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InfoTeam`(IN `Tea` VARCHAR(20))
    NO SQL
BEGIN

IF (SELECT COUNT(*) FROM Teams WHERE `NameTeam`=Tea )=1 THEN

	SELECT `NameTeam`, `URL` FROM `Teams` WHERE `NameTeam`=Tea;

ELSE

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TEAM NOT PRESENT';

END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitDisp`()
    NO SQL
BEGIN
	INSERT INTO `DisplayScores` (`IdCheck`, `IdMatch`, `IdOneOrTwo`, `Set1`, `Set2`, `Set3`, `Set4`, `Set5`, `Score`, `Service`, `Ref`) VALUES
(1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(3, 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(4, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(5, 3, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(6, 3, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(7, 4, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(8, 4, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(9, 5, 97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(10, 5, 98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(11, 6, 99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(12, 6, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(13, 7, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(14, 7, 102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(15, 8, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(16, 8, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(17, 9, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(18, 9, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(19, 10, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(20, 10, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(21, 11, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(22, 11, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(23, 12, 103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(24, 12, 104, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(25, 13, 105, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(26, 13, 106, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(27, 14, 107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(28, 14, 108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(29, 15, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(30, 15, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(31, 16, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(32, 16, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(33, 17, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(34, 17, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(35, 18, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(36, 18, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(37, 19, 109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(38, 19, 110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(39, 20, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(40, 20, 112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(41, 21, 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(42, 21, 114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(43, 22, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(44, 22, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(45, 23, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(46, 23, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(47, 24, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(48, 24, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(49, 25, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(50, 25, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(51, 26, 115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(52, 26, 116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(53, 27, 117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(54, 27, 118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(55, 28, 119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(56, 28, 120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(57, 29, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(58, 29, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(59, 30, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(60, 30, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(61, 31, 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(62, 31, 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(63, 32, 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(64, 32, 40, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(65, 33, 121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(66, 33, 122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(67, 34, 123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(68, 34, 124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(69, 35, 125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(70, 35, 126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(71, 36, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(72, 36, 42, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(73, 37, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(74, 37, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(75, 38, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(76, 38, 46, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(77, 39, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(78, 39, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(79, 40, 127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(80, 40, 128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(81, 41, 129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(82, 41, 130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(83, 42, 131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(84, 42, 132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(85, 43, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(86, 43, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(87, 44, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(88, 44, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(89, 45, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(90, 45, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(91, 46, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(92, 46, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(93, 47, 133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(94, 47, 134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(95, 48, 135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(96, 48, 136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(97, 49, 137, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(98, 49, 138, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(99, 50, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(100, 50, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(101, 51, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(102, 51, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(103, 52, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(104, 52, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(105, 53, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(106, 53, 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(107, 54, 139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(108, 54, 140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(109, 55, 141, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(110, 55, 142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(111, 56, 143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(112, 56, 144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(113, 57, 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(114, 57, 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(115, 58, 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(116, 58, 68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(117, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(118, 59, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(119, 60, 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(120, 60, 72, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(121, 61, 145, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(122, 61, 146, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(123, 62, 147, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(124, 62, 148, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(125, 63, 149, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(126, 63, 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(127, 64, 73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(128, 64, 74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(129, 65, 75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(130, 65, 76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(131, 66, 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(132, 66, 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(133, 67, 79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(134, 67, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(135, 68, 151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(136, 68, 152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(137, 69, 153, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(138, 69, 154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(139, 70, 155, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(140, 70, 156, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(141, 71, 81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(142, 71, 82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(143, 72, 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(144, 72, 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(145, 73, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(146, 73, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(147, 74, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(148, 74, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(149, 75, 157, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(150, 75, 158, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(151, 76, 159, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(152, 76, 160, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(153, 77, 161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(154, 77, 162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(155, 78, 89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(156, 78, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(157, 79, 91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(158, 79, 92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(159, 80, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(160, 80, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(161, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(162, 81, 96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(163, 82, 163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(164, 82, 164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(165, 83, 165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(166, 83, 166, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(167, 84, 167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(168, 84, 168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B');

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitHugeMatch`()
    NO SQL
INSERT INTO `HugeMatch`(`IdHugeMatch`, `IdMatch`, `Team_A`, `Score_A`, `Team_B`, `Score_B`, `Tableau`) VALUES 
(1,1,NULL,0,NULL,0,'1/4-P'),
(1,2,NULL,0,NULL,0,'1/4-P'),
(1,3,NULL,0,NULL,0,'1/4-P'),
(1,4,NULL,0,NULL,0,'1/4-P'),
(1,5,NULL,0,NULL,0,'1/4-P'),
(1,6,NULL,0,NULL,0,'1/4-P'),
(1,7,NULL,0,NULL,0,'1/4-P'),

(2,8,NULL,0,NULL,0,'1/4-P'),
(2,9,NULL,0,NULL,0,'1/4-P'),
(2,10,NULL,0,NULL,0,'1/4-P'),
(2,11,NULL,0,NULL,0,'1/4-P'),
(2,12,NULL,0,NULL,0,'1/4-P'),
(2,13,NULL,0,NULL,0,'1/4-P'),
(2,14,NULL,0,NULL,0,'1/4-P'),

(3,15,NULL,0,NULL,0,'1/4-P'),
(3,16,NULL,0,NULL,0,'1/4-P'),
(3,17,NULL,0,NULL,0,'1/4-P'),
(3,18,NULL,0,NULL,0,'1/4-P'),
(3,19,NULL,0,NULL,0,'1/4-P'),
(3,20,NULL,0,NULL,0,'1/4-P'),
(3,21,NULL,0,NULL,0,'1/4-P'),

(4,22,NULL,0,NULL,0,'1/4-P'),
(4,23,NULL,0,NULL,0,'1/4-P'),
(4,24,NULL,0,NULL,0,'1/4-P'),
(4,25,NULL,0,NULL,0,'1/4-P'),
(4,26,NULL,0,NULL,0,'1/4-P'),
(4,27,NULL,0,NULL,0,'1/4-P'),
(4,28,NULL,0,NULL,0,'1/4-P'),



(5,29,NULL,0,NULL,0,'1/2-P'),
(5,30,NULL,0,NULL,0,'1/2-P'),
(5,31,NULL,0,NULL,0,'1/2-P'),
(5,32,NULL,0,NULL,0,'1/2-P'),
(5,33,NULL,0,NULL,0,'1/2-P'),
(5,34,NULL,0,NULL,0,'1/2-P'),
(5,35,NULL,0,NULL,0,'1/2-P'),

(6,36,NULL,0,NULL,0,'1/2-P'),
(6,37,NULL,0,NULL,0,'1/2-P'),
(6,38,NULL,0,NULL,0,'1/2-P'),
(6,39,NULL,0,NULL,0,'1/2-P'),
(6,40,NULL,0,NULL,0,'1/2-P'),
(6,41,NULL,0,NULL,0,'1/2-P'),
(6,42,NULL,0,NULL,0,'1/2-P'),

(7,43,NULL,0,NULL,0,'1/2-5'),
(7,44,NULL,0,NULL,0,'1/2-5'),
(7,45,NULL,0,NULL,0,'1/2-5'),
(7,46,NULL,0,NULL,0,'1/2-5'),
(7,47,NULL,0,NULL,0,'1/2-5'),
(7,48,NULL,0,NULL,0,'1/2-5'),
(7,49,NULL,0,NULL,0,'1/2-5'),

(8,50,NULL,0,NULL,0,'1/2-5'),
(8,51,NULL,0,NULL,0,'1/2-5'),
(8,52,NULL,0,NULL,0,'1/2-5'),
(8,53,NULL,0,NULL,0,'1/2-5'),
(8,54,NULL,0,NULL,0,'1/2-5'),
(8,55,NULL,0,NULL,0,'1/2-5'),
(8,56,NULL,0,NULL,0,'1/2-5'),



(9,57,NULL,0,NULL,0,'Final-P'),
(9,58,NULL,0,NULL,0,'Final-P'),
(9,59,NULL,0,NULL,0,'Final-P'),
(9,60,NULL,0,NULL,0,'Final-P'),
(9,61,NULL,0,NULL,0,'Final-P'),
(9,62,NULL,0,NULL,0,'Final-P'),
(9,63,NULL,0,NULL,0,'Final-P'),

(10,64,NULL,0,NULL,0,'Final-3'),
(10,65,NULL,0,NULL,0,'Final-3'),
(10,66,NULL,0,NULL,0,'Final-3'),
(10,67,NULL,0,NULL,0,'Final-3'),
(10,68,NULL,0,NULL,0,'Final-3'),
(10,69,NULL,0,NULL,0,'Final-3'),
(10,70,NULL,0,NULL,0,'Final-3'),

(11,71,NULL,0,NULL,0,'Final-5'),
(11,72,NULL,0,NULL,0,'Final-5'),
(11,73,NULL,0,NULL,0,'Final-5'),
(11,74,NULL,0,NULL,0,'Final-5'),
(11,75,NULL,0,NULL,0,'Final-5'),
(11,76,NULL,0,NULL,0,'Final-5'),
(11,77,NULL,0,NULL,0,'Final-5'),

(12,78,NULL,0,NULL,0,'Final-7'),
(12,79,NULL,0,NULL,0,'Final-7'),
(12,80,NULL,0,NULL,0,'Final-7'),
(12,81,NULL,0,NULL,0,'Final-7'),
(12,82,NULL,0,NULL,0,'Final-7'),
(12,83,NULL,0,NULL,0,'Final-7'),
(12,84,NULL,0,NULL,0,'Final-7')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitMatchs`()
    NO SQL
INSERT INTO `Matchs`(`IdMatch`, `IdHugeMatch`, `Category`, `IdOneOrTwo1`, `IdOneOrTwo2`, `Court`, `DateStart`, `Statut`, `Winner`) VALUES 
(1,1,'SM',1,2,NULL,NULL,'SOON',NULL),
(2,1,'SM',3,4,NULL,NULL,'SOON',NULL),
(3,1,'SW',5,6,NULL,NULL,'SOON',NULL),
(4,1,'SW',7,8,NULL,NULL,'SOON',NULL),
(5,1,'DM',97,98,NULL,NULL,'SOON',NULL),
(6,1,'DW',99,100,NULL,NULL,'SOON',NULL),
(7,1,'DX',101,102,NULL,NULL,'SOON',NULL),
(8,2,'SM',9,10,NULL,NULL,'SOON',NULL),
(9,2,'SM',11,12,NULL,NULL,'SOON',NULL),
(10,2,'SW',13,14,NULL,NULL,'SOON',NULL),
(11,2,'SW',15,16,NULL,NULL,'SOON',NULL),
(12,2,'DM',103,104,NULL,NULL,'SOON',NULL),
(13,2,'DW',105,106,NULL,NULL,'SOON',NULL),
(14,2,'DX',107,108,NULL,NULL,'SOON',NULL),
(15,3,'SM',17,18,NULL,NULL,'SOON',NULL),
(16,3,'SM',19,20,NULL,NULL,'SOON',NULL),
(17,3,'SW',21,22,NULL,NULL,'SOON',NULL),
(18,3,'SW',23,24,NULL,NULL,'SOON',NULL),
(19,3,'DM',109,110,NULL,NULL,'SOON',NULL),
(20,3,'DW',111,112,NULL,NULL,'SOON',NULL),
(21,3,'DX',113,114,NULL,NULL,'SOON',NULL),
(22,4,'SM',25,26,NULL,NULL,'SOON',NULL),
(23,4,'SM',27,28,NULL,NULL,'SOON',NULL),
(24,4,'SW',29,30,NULL,NULL,'SOON',NULL),
(25,4,'SW',31,32,NULL,NULL,'SOON',NULL),
(26,4,'DM',115,116,NULL,NULL,'SOON',NULL),
(27,4,'DW',117,118,NULL,NULL,'SOON',NULL),
(28,4,'DX',119,120,NULL,NULL,'SOON',NULL),

(29,5,'SM',33,34,NULL,NULL,'SOON',NULL),
(30,5,'SM',35,36,NULL,NULL,'SOON',NULL),
(31,5,'SW',37,38,NULL,NULL,'SOON',NULL),
(32,5,'SW',39,40,NULL,NULL,'SOON',NULL),
(33,5,'DM',121,122,NULL,NULL,'SOON',NULL),
(34,5,'DW',123,124,NULL,NULL,'SOON',NULL),
(35,5,'DX',125,126,NULL,NULL,'SOON',NULL),
(36,6,'SM',41,42,NULL,NULL,'SOON',NULL),
(37,6,'SM',43,44,NULL,NULL,'SOON',NULL),
(38,6,'SW',45,46,NULL,NULL,'SOON',NULL),
(39,6,'SW',47,48,NULL,NULL,'SOON',NULL),
(40,6,'DM',127,128,NULL,NULL,'SOON',NULL),
(41,6,'DW',129,130,NULL,NULL,'SOON',NULL),
(42,6,'DX',131,132,NULL,NULL,'SOON',NULL),
(43,7,'SM',49,50,NULL,NULL,'SOON',NULL),
(44,7,'SM',51,52,NULL,NULL,'SOON',NULL),
(45,7,'SW',53,54,NULL,NULL,'SOON',NULL),
(46,7,'SW',55,56,NULL,NULL,'SOON',NULL),
(47,7,'DM',133,134,NULL,NULL,'SOON',NULL),
(48,7,'DW',135,136,NULL,NULL,'SOON',NULL),
(49,7,'DX',137,138,NULL,NULL,'SOON',NULL),
(50,8,'SM',57,58,NULL,NULL,'SOON',NULL),
(51,8,'SM',59,60,NULL,NULL,'SOON',NULL),
(52,8,'SW',61,62,NULL,NULL,'SOON',NULL),
(53,8,'SW',63,64,NULL,NULL,'SOON',NULL),
(54,8,'DM',139,140,NULL,NULL,'SOON',NULL),
(55,8,'DW',141,142,NULL,NULL,'SOON',NULL),
(56,8,'DX',143,144,NULL,NULL,'SOON',NULL),

(57,9,'SM',65,66,NULL,NULL,'SOON',NULL),
(58,9,'SM',67,68,NULL,NULL,'SOON',NULL),
(59,9,'SW',69,70,NULL,NULL,'SOON',NULL),
(60,9,'SW',71,72,NULL,NULL,'SOON',NULL),
(61,9,'DM',145,146,NULL,NULL,'SOON',NULL),
(62,9,'DW',147,148,NULL,NULL,'SOON',NULL),
(63,9,'DX',149,150,NULL,NULL,'SOON',NULL),
(64,10,'SM',73,74,NULL,NULL,'SOON',NULL),
(65,10,'SM',75,76,NULL,NULL,'SOON',NULL),
(66,10,'SW',77,78,NULL,NULL,'SOON',NULL),
(67,10,'SW',79,80,NULL,NULL,'SOON',NULL),
(68,10,'DM',151,152,NULL,NULL,'SOON',NULL),
(69,10,'DW',153,154,NULL,NULL,'SOON',NULL),
(70,10,'DX',155,156,NULL,NULL,'SOON',NULL),
(71,11,'SM',81,82,NULL,NULL,'SOON',NULL),
(72,11,'SM',83,84,NULL,NULL,'SOON',NULL),
(73,11,'SW',85,86,NULL,NULL,'SOON',NULL),
(74,11,'SW',87,88,NULL,NULL,'SOON',NULL),
(75,11,'DM',157,158,NULL,NULL,'SOON',NULL),
(76,11,'DW',159,160,NULL,NULL,'SOON',NULL),
(77,11,'DX',161,162,NULL,NULL,'SOON',NULL),
(78,12,'SM',89,90,NULL,NULL,'SOON',NULL),
(79,12,'SM',91,92,NULL,NULL,'SOON',NULL),
(80,12,'SW',93,94,NULL,NULL,'SOON',NULL),
(81,12,'SW',95,96,NULL,NULL,'SOON',NULL),
(82,12,'DM',163,164,NULL,NULL,'SOON',NULL),
(83,12,'DW',165,166,NULL,NULL,'SOON',NULL),
(84,12,'DX',167,168,NULL,NULL,'SOON',NULL)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitOneOrTwo`()
    NO SQL
INSERT INTO `OneOrTwo`(`IdPlayer_A`, `IdPlayer_B`, `Category`) VALUES 
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Simple'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double'),
(NULL,NULL,'Double')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitPlayer`()
    NO SQL
INSERT INTO `Players` (`IdPlayer`, `Team`, `Name`, `FirstName`, `Sex`, `DateBirth`) VALUES
(1, 'BEL', 'CANT', 'Louis', 'M', '1990-11-08'),
(2, 'BEL', 'STORME', 'James Junior', 'M', '1994-12-13'),
(3, 'BEL', 'MERCKX', 'Jonas', 'M', '1993-10-04'),
(4, 'BEL', 'DELEFORTRIE', 'Anouk', 'W', '1990-11-29'),
(5, 'BEL', 'DE SUTTER', 'Justine', 'W', '1995-03-20'),
(6, 'BEL', 'DOM', 'Evelyne', 'W', ''),

(7, 'CHN', 'ZHU', 'Ai Wen', 'W', '1994-09-22'),
(8, 'CHN', 'YANG', 'Yi', 'W', '1989-09-27'),
(9, 'CHN', 'WANG', 'Yue', 'W', '1994-01-23'),
(10, 'CHN', 'ZHONG', 'Su Hao', 'M', '1997-03-19'),
(11, 'CHN', 'PENG', 'Changwei', 'M', '1994-02-16'),
(12, 'CHN', 'CAI', 'Zhao', 'M', '1994-07-05'),

(13, 'USA', 'VAN NGUYEN', 'Chanelle', 'W', '1994-01-19'),
(14, 'USA', 'RUBIN', 'Noah', 'M', '1996-02-21'),
(15, 'USA', 'PASHA', 'Teahan', 'M', '1992-07-15'),
(16, 'USA', 'KWIATKOWSKI', 'Thai', 'M', '1995-02-13'),
(17, 'USA', 'ELBABA', 'Julia', 'W', '1994-06-13'),
(18, 'USA', 'ANDERSON', 'Robin', 'W', '1993-04-12'),

(19, 'DEU', 'STEMMER', 'Stefanie', 'W', '1993-08-03'),
(20, 'DEU', 'FUCHS', 'Anna­Benita', 'W', '1992-11-06'),
(21, 'DEU', 'SCHELENZ', 'Desiree', 'W', ''),
(22, 'DEU', 'REGUS', 'Ralph', 'M', '1990-07-18'),
(23, 'DEU', 'WETZEL', 'Mattis', 'M', '1989-11-21'),
(24, 'DEU', 'DORNBUSCH', 'Michel', 'M', '1990-09-30'),

(25, 'GBR', 'WALSH', 'Darren', 'M', '1989-01-16'),
(26, 'GBR', 'FITZPATRICK', 'Anna', 'W', '1989-04-06'),
(27, 'GBR', 'EECKELAERS', 'Nicholas', 'M', '1993-12-11'),
(28, 'GBR', 'WALKER', 'Alexandra', 'W', '1992-09-21'),
(29, 'GBR', 'REN', 'Jessica', 'W', '1994-08-23'),
(30, 'GBR', 'WHITHEHOUSE', 'Mark', 'M', '1993-05-01'),

(31, 'IRL', 'REID', 'Cian', 'M', '1995-06-08'),
(32, 'IRL', 'SHEEHAN', 'Phillip', 'M', '1996-01-15'),
(33, 'IRL', 'BYRNE', 'Julie', 'W', '1997-07-16'),
(34, 'IRL', 'BENJENARU', 'Carola', 'W', '1997-06-29'),
(35, 'IRL', 'KENNEDY', 'Sinead', 'W', '1993-10-18'),
(36, 'IRL', 'HUGHES', 'Jack', 'M', '1991-04-23'),

(37, 'RUS', 'DANILINA', 'Anna', 'W', '1995-08-20'),
(38, 'RUS', 'ELISTRATOV', 'Evgeny', 'M', '1994-12-28'),
(39, 'RUS', 'KOZYUKOV', 'Vitaly', 'M', '1994-06-02'),
(40, 'RUS', 'VALETOVA', 'Iuliia', 'W', '1994-07-18'),
(41, 'RUS', 'KAMENSKAYA', 'Victoria', 'W', '1991-11-26'),
(42, 'RUS', 'POLYAKOV', 'Vladimir', 'M', '1994-06-18'),

(43, 'FRA', 'HOANG', 'Antoine', 'M', '1995-11-04'),
(44, 'FRA', 'REBOUL', 'Fabien', 'M', '1995-09-09'),
(45, 'FRA', 'JACQ', 'Grégoire', 'M', '1992-11-09'),
(46, 'FRA', 'BACQUIE', 'Alice', 'W', '1995-07-23'),
(47, 'FRA', 'GIRARD', 'Rachel', 'W', '1994-01-10'),
(48, 'FRA', 'LARRIERE', 'Victoria', 'W', '1991-05-02')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitPlayerTest`()
    NO SQL
INSERT INTO `Players` (`IdPlayer`, `Team`, `Name`, `FirstName`, `Sex`, `DateBirth`) VALUES
(1, 'BEL', 'ALLON', 'LEVY', 'W', '1990-01-01'),
(2, 'BEL', 'BACARD', 'HUGO', 'M', '1990-01-01'),
(3, 'BEL', 'BAKER', 'MATTHEW', 'M', '1990-01-01'),
(4, 'BEL', 'BALWE', 'CHETAN', 'W', '1990-01-01'),
(5, 'BEL', 'BRUGALLE', 'ERWAN', 'M', '1990-01-01'),
(6, 'BEL', 'BYRON', 'DYLAN', 'M', '1990-01-01'),
(7, 'BEL', 'CEBALLOS', 'CESAR', 'M', '1990-01-01'),
(8, 'CHN', 'CHATZIDZAKIS', 'ZOE', 'W', '1990-01-01'),
(9, 'CHN', 'CHEN', 'KE', 'M', '1990-01-01'),
(10, 'CHN', 'CHRISTIE', 'AARON', 'M', '1990-01-01'),
(11, 'CHN', 'BLANCO', 'JESUS', 'M', '1990-01-01'),
(12, 'CHN', 'DELLO', 'PIETRO', 'M', '1990-01-01'),
(13, 'CHN', 'DISEGNI', 'DANIEL', 'M', '1990-01-01'),
(14, 'CHN', 'FARANG-HARIRI', 'BANAFSHEH', 'M', '1990-01-01'),
(15, 'USA', 'FEHM', 'ARNO', 'M', '1990-01-01'),
(16, 'USA', 'FORNASIERO', 'ANTONGIULIO', 'M', '1990-01-01'),
(17, 'USA', 'GARAY-LOPEZ', 'CHRISTIAN', 'M', '1990-01-01'),
(18, 'USA', 'HABICH', 'MATHIAS', 'M', '1990-01-01'),
(19, 'USA', 'HAJLI', 'MOUNIR', 'M', '1990-01-01'),
(20, 'USA', 'HILS', 'MARTIN', 'M', '1990-01-01'),
(21, 'USA', 'HSIA', 'CHUNG', 'W', '1990-01-01'),
(22, 'DEU', 'IZHAKIAN', 'ZUR', 'W', '1990-01-01'),
(23, 'DEU', 'JONSONN', 'MATTHIAS', 'M', '1990-01-01'),
(24, 'DEU', 'LAGERBERG', 'ARON', 'M', '1990-01-01'),
(25, 'DEU', 'LANG', 'LIONEL', 'M', '1990-01-01'),
(26, 'DEU', 'LI', 'WEN-WEI', 'W', '1990-01-01'),
(27, 'DEU', 'MACULAN', 'MARCO', 'M', '1990-01-01'),
(28, 'DEU', 'MANTOVAN', 'ELENA', 'W', '1990-01-01'),
(29, 'GBR', 'MOUSSAOUI', 'AHMED', 'M', '1990-01-01'),
(30, 'GBR', 'NAHID', 'SHAJARI', 'M', '1990-01-01'),
(31, 'GBR', 'NICAISE', 'JOHANNES', 'W', '1990-01-01'),
(32, 'GBR', 'NISSE', 'MOUNIR', 'M', '1990-01-01'),
(33, 'GBR', 'PAUGAM', 'FREDERIC', 'M', '1990-01-01'),
(34, 'GBR', 'PAYNE', 'SAM', 'M', '1990-01-01'),
(35, 'GBR', 'PETSCHE', 'CLAYTON', 'M', '1990-01-01'),
(36, 'IRL', 'PINEIRO', 'JORGE', 'M', '1990-01-01'),
(37, 'IRL', 'REMY', 'BERTRAND', 'M', '1990-01-01'),
(38, 'IRL', 'RYDH', 'PIERRE', 'M', '1990-01-01'),
(39, 'IRL', 'SCHMIDT', 'NICOLAS', 'M', '1990-01-01'),
(40, 'IRL', 'SHAW', 'KRISTIN', 'W', '1990-01-01'),
(41, 'IRL', 'SMEETS', 'ARNE', 'W', '1990-01-01'),
(42, 'IRL', 'SOTO', 'CESAR', 'M', '1990-01-01'),
(43, 'RUS', 'SUN', 'FEI', 'W', '1990-01-01'),
(44, 'RUS', 'SZPIRO', 'LUCIEN', 'M', '1990-01-01'),
(45, 'RUS', 'TEISSIER', 'BERNARD', 'M', '1990-01-01'),
(46, 'RUS', 'TEMKIN', 'MICKAEL', 'M', '1990-01-01'),
(47, 'RUS', 'TRUCCO', 'Eugenio', 'M', '1990-01-01'),
(48, 'RUS', 'TUCKER', 'THOMAS', 'M', '1990-01-01'),
(49, 'RUS', 'TYOMKIM', 'ILYA', 'W', '1990-01-01'),
(50, 'FRA', 'VAQUIE', 'MICHEL', 'M', '1990-01-01'),
(51, 'FRA', 'VEZZANI', 'ALBERTO', 'M', '1990-01-01'),
(52, 'FRA', 'VIGNERAS', 'MARIE', 'W', '1990-01-01'),
(53, 'FRA', 'WAGNER', 'TILL', 'M', '1990-01-01'),
(54, 'FRA', 'YALKINOGLU', 'BORA', 'M', '1990-01-01'),
(55, 'FRA', 'YAMATI', 'KAZUHIKO', 'W', '1990-01-01'),
(56, 'FRA', 'YIN', 'YIMU', 'W', '1990-01-01')$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InitTeams`()
    NO SQL
INSERT INTO `Teams` (`NameTeam`, `IdTeam`, `CompleteName`, `Points`, `URL`) VALUES
('Equipe1', 1, NULL, 0, NULL),
('Equipe2', 2, NULL, 0, NULL),
('Equipe3', 3, NULL, 0, NULL),
('Equipe4', 4, NULL, 0, NULL),
('Equipe5', 5, NULL, 0, NULL),
('Equipe6', 6, NULL, 0, NULL),
('Equipe7', 7, NULL, 0, NULL),
('Equipe8', 8, NULL, 0, NULL)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertLog`(IN `Idp` INT, IN `idmatch` INT, IN `setA` INT, IN `setB` INT, IN `gameA` INT, IN `gameB` INT, IN `scoreA` INT, IN `scoreB` INT, IN `service` BOOLEAN, IN `re` VARCHAR(2), IN `stat` VARCHAR(5), IN `FsFault` INT, IN `Bre` INT)
    NO SQL
INSERT INTO `MASTERU-TENNIS`.`Log`(
    `Id`,
    `IdMatch`, 
    `SetA`, 
    `SetB`, 
    `GameA`, 
    `GameB`, 
    `ScoreA`, 
    `ScoreB`, 
    `Service`,
    `Ref`,
    `Statistic`,
    `FSF`,
    `Break`
)
VALUES (Idp ,idmatch, setA, setB, gameA, gameB, scoreA, scoreB, service,re,stat,FsFault,Bre)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertUsers`(IN `usrname` VARCHAR(50), IN `mdp` VARCHAR(50))
BEGIN

IF ((SELECT COUNT(*) FROM Users WHERE Username = usrname) > 0) THEN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'USERNAME ALREADY EXIST';
ELSE
	INSERT INTO Users VALUES (usrname, SHA2(mdp,256));
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListMatchs`(IN `Tab` ENUM('1/4','1/2','Final'))
    NO SQL
BEGIN

CASE tab
	
	WHEN '1/4' THEN
		SELECT 
			Matchs.IdMatch, 
			Team_A, 
			(SELECT GROUP_CONCAT((CONCAT_WS (' ',Name,FirstName)) SEPARATOR ' / ') 
				FROM Players, OneOrTwo 
				WHERE IdPlayer 
				IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1
			)
		AS 
			Name_Complet_A,  
			Team_B, 
			(SELECT GROUP_CONCAT((CONCAT_WS (' ',Name,FirstName)) SEPARATOR ' / ') 
				FROM Players, OneOrTwo 
				WHERE IdPlayer 
				IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2
			) 
		AS 
			Name_Complet_B,  
			Category,
			Court,
			Tableau, 
			Statut 
		FROM Matchs, HugeMatch, Players
		WHERE
			HugeMatch.IdHugeMatch = Matchs.IdHugeMatch 
			AND Tableau='1/4-P'
		GROUP BY Matchs.IdMatch;
	
	WHEN '1/2' THEN
		SELECT 
			Matchs.IdMatch, 
			Team_A, 
			(SELECT GROUP_CONCAT((CONCAT_WS (' ',Name,FirstName)) SEPARATOR ' / ') 
				FROM Players, OneOrTwo 
				WHERE IdPlayer 
				IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1
			)
		AS 
			Name_Complet_A,  
			Team_B, 
			(SELECT GROUP_CONCAT((CONCAT_WS (' ',Name,FirstName)) SEPARATOR ' / ') 
				FROM Players, OneOrTwo 
				WHERE IdPlayer 
				IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2
			) 
		AS 
			Name_Complet_B,  
			Category,
			Court,
			Tableau, 
			Statut 
		FROM Matchs, HugeMatch, Players
		WHERE
		 	HugeMatch.IdHugeMatch = Matchs.IdHugeMatch 
			AND ( Tableau='1/2-P' OR Tableau='1/2-5' )
		GROUP BY Matchs.IdMatch;

	WHEN 'Final' THEN
		SELECT 
			Matchs.IdMatch, 
			Team_A, 
			(SELECT GROUP_CONCAT((CONCAT_WS (' ',Name,FirstName)) SEPARATOR ' / ') 
				FROM Players, OneOrTwo 
				WHERE IdPlayer 
				IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo1
			)
		AS 
			Name_Complet_A,  
			Team_B, 
			(SELECT GROUP_CONCAT((CONCAT_WS (' ',Name,FirstName)) SEPARATOR ' / ') 
				FROM Players, OneOrTwo 
				WHERE IdPlayer 
				IN (OneOrTwo.IdPlayer_A, OneOrTwo.IdPlayer_B) AND IdOneOrTwo = IdOneOrTwo2
			) 
		AS 
			Name_Complet_B,  
			Category,
			Court,
			Tableau, 
			Statut 
		FROM Matchs, HugeMatch, Players
		WHERE 
			HugeMatch.IdHugeMatch = Matchs.IdHugeMatch 
			AND ( Tableau='Final-P' OR Tableau='Final-3' OR Tableau='Final-5' OR Tableau='Final-7' )
		GROUP BY Matchs.IdMatch;
END CASE;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListsPlayers`()
    NO SQL
SELECT `IdPlayer`, `Team`, `Name`, `FirstName`, `Sex`, `DateBirth`, `Url` FROM `Players`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `ListsPlayersTeam`(IN `pays` VARCHAR(20), IN `se` ENUM('M','W','X'))
    NO SQL
BEGIN

	CASE se
	
		WHEN 'M' THEN

			SELECT IdPlayer,Team , Name, FirstName, Sex, DateBirth, Url FROM Players WHERE Team = pays AND Sex = 'M';

		WHEN 'W' THEN

			SELECT IdPlayer,Team , Name, FirstName, Sex, DateBirth, Url FROM Players WHERE Team = pays AND Sex = 'W';
	
		WHEN 'X' THEN

			SELECT IdPlayer,Team , Name, FirstName, Sex, DateBirth, Url FROM Players WHERE Team = pays;

	END CASE;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Login`(IN `usrname` VARCHAR(50), IN `mdp` TEXT)
BEGIN

DECLARE tmpTable varchar(256);

SELECT Password INTO tmpTable FROM Users WHERE UserName = usrname;

IF ((SELECT SHA2(mdp, 256)) = tmpTable) THEN
	SELECT 'OK' as success;
ELSE
	SELECT 'KO' as success;	
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `NextHugeMatch`(IN `Stag` VARCHAR(20))
    COMMENT 'Programmation des matchs'
BEGIN

DECLARE s1, s2 INT;
DECLARE n1, n2 VARCHAR(20);

CASE Stag
	WHEN 'SETTING-UP' THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SETTING-UP';
		
	WHEN '1/4' THEN
		UPDATE `HugeMatch` SET `HugeStatut`='PROGRESS' WHERE IdHugeMatch < 5;
		
	WHEN '1/2' THEN
	/* IdHugeMatch = 1 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 1;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 1 AND IdMatch = 1;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 5;
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 7;
		ELSE
            
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 5;
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 7;
		END IF;	
		CALL UpdHugeStatut(5,29);
		CALL UpdHugeStatut(7,43);
	/* IdHugeMatch = 2 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 2;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 2 AND IdMatch = 8;
	
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 5;
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 7;
		ELSE
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 5;
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 7;
		END IF;	
		CALL UpdHugeStatut(5,29);
		CALL UpdHugeStatut(7,43);
	/* IdHugeMatch = 3 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 3;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 3 AND IdMatch = 15;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 6;
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 8;
		ELSE
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 6;
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 8;
		END IF;	
		CALL UpdHugeStatut(6,36);
		CALL UpdHugeStatut(8,50);
	/* IdHugeMatch = 4 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 4;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 4 AND IdMatch = 22;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 6;
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 8;
		ELSE
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 6;
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 8;
		END IF;	
		CALL UpdHugeStatut(6,36);
		CALL UpdHugeStatut(8,50);
		
	WHEN 'Final' THEN
	/* IdHugeMatch = 5 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 5;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 5 AND IdMatch = 29;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 9;
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 10;
		ELSE
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 9;
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 10;
		END IF;	
		CALL UpdHugeStatut(9,57);
		CALL UpdHugeStatut(10,64);
	/* IdHugeMatch = 6 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 6;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 6 AND IdMatch = 36;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 9;
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 10;
		ELSE
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 9;
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 10;
		END IF;	
		CALL UpdHugeStatut(9,57);
		CALL UpdHugeStatut(10,64);
	/* IdHugeMatch = 7 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 7;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 7 AND IdMatch = 43;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 11;
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 12;
		ELSE
			UPDATE HugeMatch SET Team_A = n2 WHERE IdHugeMatch = 11;
			UPDATE HugeMatch SET Team_A = n1 WHERE IdHugeMatch = 12;
		END IF;	
		CALL UpdHugeStatut(11,71);
		CALL UpdHugeStatut(12,78);
	/* IdHugeMatch = 8 */
		SELECT SUM(Score_A), SUM(Score_B) INTO s1, s2 FROM `HugeMatch` WHERE IdHugeMatch = 8;
		SELECT Team_A, Team_B INTO n1, n2 FROM `HugeMatch` WHERE IdHugeMatch = 8 AND IdMatch = 50;
		
		IF (s1 > s2) THEN
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 11;
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 12;
		ELSE
			UPDATE HugeMatch SET Team_B = n2 WHERE IdHugeMatch = 11;
			UPDATE HugeMatch SET Team_B = n1 WHERE IdHugeMatch = 12;
		END IF;	
		CALL UpdHugeStatut(11,71);
		CALL UpdHugeStatut(12,78);
		
END CASE;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Reset`()
    MODIFIES SQL DATA
BEGIN

UPDATE `DisplayScores` SET `Set1`=NULL,`Set2`=NULL,`Set3`=NULL,`Set4`=NULL,`Set5`=NULL,`Score`=NULL,`Service`=NULL;
UPDATE `OneOrTwo` SET `IdPlayer_A`=NULL,`IdPlayer_B`=NULL;
DELETE FROM `Players`;
ALTER TABLE `Players` AUTO_INCREMENT = 1;
DELETE FROM `Log`;
DELETE FROM `StatMatch`;

UPDATE `Matchs` SET `Court`=NULL,`DateStart`=NULL,`DateEnd`=NULL,`Statut`='SOON',`Winner`=NULL;
UPDATE `HugeMatch` SET `HugeStatut`='SOON',`Team_A`=NULL,`Score_A`=0,`Team_B`=NULL,`Score_B`=0;

UPDATE Tournament SET `Stage` = 'SETTING-UP';

UPDATE `Teams` SET `NameTeam`= 'Equipe1',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 1;
UPDATE `Teams` SET `NameTeam`= 'Equipe2',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 2;
UPDATE `Teams` SET `NameTeam`= 'Equipe3',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 3;
UPDATE `Teams` SET `NameTeam`= 'Equipe4',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 4;
UPDATE `Teams` SET `NameTeam`= 'Equipe5',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 5;
UPDATE `Teams` SET `NameTeam`= 'Equipe6',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 6;
UPDATE `Teams` SET `NameTeam`= 'Equipe7',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 7;
UPDATE `Teams` SET `NameTeam`= 'Equipe8',`CompleteName` = NULL,`Points` = 0, `URL`= NULL WHERE IdTeam = 8;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SelectLog`(IN `idm` INT)
    NO SQL
    COMMENT 'Procédure sur Points pour récupérer SetA/B, GameA/B, ScoreA/B...'
BEGIN
IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	SELECT `SetA`, `SetB`, `GameA`, `GameB`, `ScoreA`, `ScoreB`, `Service` FROM `Log` WHERE `Id` IN (SELECT MAX(Id) FROM `Log` WHERE IdMatch=idm);
ELSE 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NON CREE';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetUrlPlayer`(IN `IdP` INT, IN `Ur` TEXT)
    NO SQL
BEGIN
IF ( (SELECT COUNT(*) FROM Players  WHERE IdPlayer=IdP) = 1 )
THEN
	UPDATE `Players` SET `Url`=Ur WHERE `IdPlayer`=IdP;
ELSE 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'PLAYER NOT PRESENT';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SetUrlTeam`(IN `Tea` VARCHAR(20), IN `Ur` TEXT)
    NO SQL
Begin
IF (SELECT COUNT(*) FROM Teams WHERE `NameTeam`=Tea )=1 THEN
	UPDATE `Teams` SET `URL`=ur WHERE `NameTeam`=Tea;
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TEAM NOT CREATE';
END IF;
End$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `StartMatch`(IN `IdM` INT, IN `NumCourt` ENUM('1','2','3','4','5','6','7','8'), IN `Start` DATETIME, IN `Service` BOOLEAN, IN `IdPA_OoT1` INT, IN `IdPB_OoT1` INT, IN `IdPA_OoT2` INT, IN `IdPB_OoT2` INT)
    MODIFIES SQL DATA
BEGIN


DECLARE stat VARCHAR(10);
DECLARE catMat VARCHAR(3);
DECLARE catOoT1 VARCHAR(10);
DECLARE catOoT2 VARCHAR(10);
DECLARE TeaOoT1 VARCHAR(10);
DECLARE TeaOoT2 VARCHAR(10);
IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	SELECT `Statut` INTO stat FROM `Matchs` WHERE `IdMatch` = idm;

	IF (stat = 'SOON') THEN
		IF (SELECT COUNT(*) FROM `Matchs` WHERE court=NumCourt and  Statut ='PROGRESS')=0 THEN
			SELECT `Category` INTO catMat FROM `Matchs` WHERE `IdMatch` = idm;
			SELECT `Category` INTO catOoT1 FROM `OneOrTwo` WHERE `IdOneOrTwo` = (SELECT `IdOneOrTwo1` FROM `Matchs` WHERE IdMatch=idm);
			SELECT `Category` INTO catOoT2 FROM `OneOrTwo` WHERE `IdOneOrTwo` = (SELECT `IdOneOrTwo2` FROM `Matchs` WHERE IdMatch=idm);
			SELECT `Team_A` INTO TeaOoT1 FROM `HugeMatch` WHERE `IdMatch` = idm;
			SELECT `Team_B` INTO TeaOoT2 FROM `HugeMatch` WHERE `IdMatch` = idm;
			
			IF catOoT1=catOoT2 THEN
				CASE catMat
					WHEN 'SM' THEN
						IF catOoT1='Simple' THEN
							IF TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT2) 
							THEN
								IF (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT1)='M'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT2)='M' 
								THEN
									CALL AffectPlayerMatch(idm,IdPA_OoT1,NULL,IdPA_OoT2,NULL);
									CALL InsertLog(0,Idm,0,0,0,0,0,0,Service,null,null,null,null);
									UPDATE `Matchs` SET `Statut`= 'PROGRESS', DateStart = Start, Court = numcourt  WHERE `IdMatch` = idm;
									CALL UpdateDisp(Idm,1,0,0,0,0,Service);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo1`FROM `Matchs` WHERE IdMatch=Idm);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo2`FROM `Matchs` WHERE IdMatch=Idm);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`,  `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Simple',IdPA_OoT1,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`,  `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Simple',IdPA_OoT2,0,0,0,0,0,0,0,0);
								ELSE
									SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SEX PLAYER NOT COMPATIBLE WITH THE CATEGORY OF THE MATCH';
								END IF;
							ELSE
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Team HUGE MATCH NOT COMPATIBLE WITH PLAYERS';
							END IF;
						ELSE
							SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'CATEGORY MATCH NOT COMPATIBLE WITH ONE_OR_TWO';
						END IF;
					WHEN 'SW' THEN
						IF catOoT1='Simple' THEN
							IF TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT2) 
							THEN
								IF (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT1)='W'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT2)='W' 
								THEN
									CALL AffectPlayerMatch(idm,IdPA_OoT1,NULL,IdPA_OoT2,NULL);
									CALL InsertLog(0,Idm,0,0,0,0,0,0,Service,null,null,null,null);
									UPDATE `Matchs` SET `Statut`= 'PROGRESS', DateStart = Start, Court = numcourt  WHERE `IdMatch` = idm;
									CALL UpdateDisp(Idm,1,0,0,0,0,Service);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo1`FROM `Matchs` WHERE IdMatch=Idm);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo2`FROM `Matchs` WHERE IdMatch=Idm);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`,  `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Simple',IdPA_OoT1,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`,  `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Simple',IdPA_OoT2,0,0,0,0,0,0,0,0);
								ELSE
									SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SEX PLAYER NOT COMPATIBLE WITH THE CATEGORY OF THE MATCH';
								END IF;
							ELSE
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Team HUGE MATCH NOT COMPATIBLE WITH PLAYERS';
							END IF;
						ELSE
							SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'CATEGORY MATCH NOT COMPATIBLE WITH ONE_OR_TWO';
						END IF;
					WHEN 'DM' THEN
						IF catOoT1='Double' THEN
							IF TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT2) 
							AND TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPB_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPB_OoT2)
							THEN
								IF (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT1)='M'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPB_OoT1)='M'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT2)='M'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPB_OoT2)='M' 
								THEN
									CALL AffectPlayerMatch(idm,IdPA_OoT1,IdPB_OoT1,IdPA_OoT2,IdPB_OoT2);
									CALL InsertLog(0,Idm,0,0,0,0,0,0,Service,null,null,null,null);
									UPDATE `Matchs` SET `Statut`= 'PROGRESS', DateStart = Start, Court = numcourt  WHERE `IdMatch` = idm;
									CALL UpdateDisp(Idm,1,0,0,0,0,Service);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo1`FROM `Matchs` WHERE IdMatch=Idm);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo2`FROM `Matchs` WHERE IdMatch=Idm);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Double',IdPA_OoT1,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Double',IdPB_OoT1,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Double',IdPA_OoT2,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Double',IdPB_OoT2,0,0,0,0,0,0,0,0);
								ELSE
									SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SEX PLAYER NOT COMPATIBLE WITH THE CATEGORY OF THE MATCH';
								END IF;
							ELSE
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Team HUGE MATCH NOT COMPATIBLE WITH PLAYERS';
							END IF;
						ELSE
							SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'CATEGORY MATCH NOT COMPATIBLE WITH ONE_OR_TWO';
						END IF;
					WHEN 'DW' THEN
						IF catOoT1='Double' THEN
							IF TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT2) 
							AND TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPB_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPB_OoT2)
							THEN
								IF (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT1)='W'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPB_OoT1)='W'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPA_OoT2)='W'
								AND (SELECT `Sex` FROM `Players` WHERE IdPlayer=IdPB_OoT2)='W' 
								THEN
									CALL AffectPlayerMatch(idm,IdPA_OoT1,IdPB_OoT1,IdPA_OoT2,IdPB_OoT2);
									CALL InsertLog(0,Idm,0,0,0,0,0,0,Service,null,null,null,null);
									UPDATE `Matchs` SET `Statut`= 'PROGRESS', DateStart = Start, Court = numcourt  WHERE `IdMatch` = idm;
									CALL UpdateDisp(Idm,1,0,0,0,0,Service);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo1`FROM `Matchs` WHERE IdMatch=Idm);
									UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo2`FROM `Matchs` WHERE IdMatch=Idm);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Double',IdPA_OoT1,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Double',IdPB_OoT1,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Double',IdPA_OoT2,0,0,0,0,0,0,0,0);
									INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Double',IdPB_OoT2,0,0,0,0,0,0,0,0);
								ELSE
									SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SEX PLAYER NOT COMPATIBLE WITH THE CATEGORY OF THE MATCH';
								END IF;
							ELSE
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Team HUGE MATCH NOT COMPATIBLE WITH PLAYERS';
							END IF;
						ELSE
							SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'CATEGORY MATCH NOT COMPATIBLE WITH ONE_OR_TWO';
						END IF;
					WHEN 'DX' THEN
						IF catOoT1='Double' THEN
							IF TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPA_OoT2) 
							AND TeaOoT1=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPB_OoT1) AND TeaOoT2=(SELECT  `Team` FROM `Players` WHERE IdPlayer=IdPB_OoT2)
							THEN
								CALL AffectPlayerMatch(idm,IdPA_OoT1,IdPB_OoT1,IdPA_OoT2,IdPB_OoT2);
								CALL InsertLog(0,Idm,0,0,0,0,0,0,Service,null,null,null,null);
								UPDATE `Matchs` SET `Statut`= 'PROGRESS', DateStart = Start, Court = numcourt  WHERE `IdMatch` = idm;
								CALL UpdateDisp(Idm,1,0,0,0,0,Service);
								UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo1`FROM `Matchs` WHERE IdMatch=Idm);
								UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE IdMatch=Idm and IdOneOrTwo=(SELECT `IdOneOrTwo2`FROM `Matchs` WHERE IdMatch=Idm);
								INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Double',IdPA_OoT1,0,0,0,0,0,0,0,0);
								INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'A','Double',IdPB_OoT1,0,0,0,0,0,0,0,0);
								INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Double',IdPA_OoT2,0,0,0,0,0,0,0,0);
								INSERT INTO `StatMatch`(`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES (IdM,'B','Double',IdPB_OoT2,0,0,0,0,0,0,0,0);
							ELSE
								SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Team HUGE MATCH NOT COMPATIBLE WITH PLAYERS';
							END IF;
						ELSE
							SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'CATEGORY MATCH NOT COMPATIBLE WITH ONE_OR_TWO';
						END IF;
				END CASE;
			ELSE
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'CATEGORY OF ONE_OR_TWO DIFFERENT';
			END IF;
		ELSE
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH IN PROGRESS ON THIS COURT';
		END IF;

	ELSE

		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH ALREADY IN PROGRESS OR FINISH'; 
	
	END IF;
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'INVALIDE NUMBER OF MATCH';
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SupprPoint`(IN `idm` INT, IN `idp` INT)
    NO SQL
BEGIN
DECLARE NumS INT;
DECLARE Se1 INT;
DECLARE Se2 INT;
DECLARE Sc1 INT;
DECLARE Sc2 INT;
DECLARE Serv INT;

IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	IF (SELECT COUNT(*) FROM Log WHERE IdMatch=Idm and Id=idp)=1 THEN
		SET NumS = (SELECT  `SetA` FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm) + (SELECT  `SetB` FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm)+1;
		SELECT  `GameA` INTO Se1 FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm;
		SELECT  `GameB` INTO Se2 FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm;
		SELECT  `ScoreA` INTO Sc1 FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm;
		SELECT  `ScoreB` INTO Sc2 FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm;
		SELECT  `Service` INTO serv FROM `Log` WHERE  `Id`=idp-1 AND `IdMatch`=idm;
		case NumS
			WHEN 1 THEN 
				UPDATE `DisplayScores` SET `Set2`=-1,`Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE  `IdMatch`= idm;
				call updateDisp(idm,NumS,Se1,Se2,Sc1,Sc2,Serv);
				DELETE FROM Log WHERE IdMatch = idm AND Id >= idp;
			WHEN 2 THEN
				UPDATE `DisplayScores` SET `Set3`=-1,`Set4`=-1,`Set5`=-1 WHERE  `IdMatch`= idm;
				call updateDisp(idm,NumS,Se1,Se2,Sc1,Sc2,Serv);
				DELETE FROM Log WHERE IdMatch = idm AND Id >= idp;
			WHEN 3 THEN 
				UPDATE `DisplayScores` SET `Set4`=-1,`Set5`=-1 WHERE  `IdMatch`= idm;
				call updateDis(idm,NumS,Se1,Se2,Sc1,Sc2,Serv);
				DELETE FROM Log WHERE IdMatch = idm AND Id >= idp;
				SELECT 'REUSSI';
			WHEN 4 THEN
				UPDATE `DisplayScores` SET `Set5`=NULL WHERE  `IdMatch`= idm;
				call updateDisp(idm,NumS,Se1,Se2,Sc1,Sc2,Serv);
				DELETE FROM Log WHERE IdMatch = idm AND Id >= idp;
			WHEN 5 THEN
				call updateDisp(idm,NumS,Se1,Se2,Sc1,Sc2,Serv);
				DELETE FROM Log WHERE IdMatch = idm AND Id >= idp;
		END CASE;
	ELSE
		SIGNAL SQLSTATE '45002' SET MESSAGE_TEXT = 'IDPOINT NOT PRESENT IN LOG';
	END IF;
ELSE
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NOT CREATE';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `TestInitTeam`()
    NO SQL
BEGIN
UPDATE `Teams` SET `NameTeam`='FRA',`CompleteName`='France' WHERE `IdTeam`=1;
UPDATE `Teams` SET `NameTeam`='CHN',`CompleteName`='Chine' WHERE `IdTeam`=2;
UPDATE `Teams` SET `NameTeam`='BEL',`CompleteName`='Belgique' WHERE `IdTeam`=3;
UPDATE `Teams` SET `NameTeam`='DEU',`CompleteName`='Allemagne' WHERE `IdTeam`=4;
UPDATE `Teams` SET `NameTeam`='RUS',`CompleteName`='Russie' WHERE `IdTeam`=5;
UPDATE `Teams` SET `NameTeam`='USA',`CompleteName`='Etat-Unis' WHERE `IdTeam`=6;
UPDATE `Teams` SET `NameTeam`='IRL',`CompleteName`='Irlande' WHERE `IdTeam`=7;
UPDATE `Teams` SET `NameTeam`='GBR',`CompleteName`='Angleterre ' WHERE `IdTeam`=8;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `TestTirageQuartFinal`()
    NO SQL
BEGIN
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=1;
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=2;
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=3;
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=4;
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=5;
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=6;
UPDATE `HugeMatch` SET `Team_A`='BEL',`Score_A`=0,`Team_B`='CHN',`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=7;

UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=8;
UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=9;
UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=10;
UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=11;
UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=12;
UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=13;
UPDATE `HugeMatch` SET `Team_A`='DEU',`Score_A`=0,`Team_B`='FRA',`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=14;

UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=15;
UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=16;
UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=17;
UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=18;
UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=19;
UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=20;
UPDATE `HugeMatch` SET `Team_A`='GBR',`Score_A`=0,`Team_B`='IRL',`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=21;

UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=22;
UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=23;
UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=24;
UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=25;
UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=26;
UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=27;
UPDATE `HugeMatch` SET `Team_A`='RUS',`Score_A`=0,`Team_B`='USA',`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=28;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Tirage`(IN `team1` VARCHAR(4), IN `team2` VARCHAR(4), IN `team3` VARCHAR(4), IN `team4` VARCHAR(4), IN `team5` VARCHAR(4), IN `team6` VARCHAR(4), IN `team7` VARCHAR(4), IN `team8` VARCHAR(4))
    MODIFIES SQL DATA
BEGIN

IF ((SELECT Stage FROM Tournament) = 'SETTING-UP') THEN

	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=1;
	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=2;
	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=3;
	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=4;
	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=5;
	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=6;
	UPDATE `HugeMatch` SET `Team_A`= team1,`Score_A`=0,`Team_B`= team2,`Score_B`=0 WHERE `IdHugeMatch`=1 AND `IdMatch`=7;

	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=8;
	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=9;
	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=10;
	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=11;
	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=12;
	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=13;
	UPDATE `HugeMatch` SET `Team_A`= team3,`Score_A`=0,`Team_B`= team4,`Score_B`=0 WHERE `IdHugeMatch`=2 AND `IdMatch`=14;

	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=15;
	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=16;
	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=17;
	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=18;
	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=19;
	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=20;
	UPDATE `HugeMatch` SET `Team_A`= team5,`Score_A`=0,`Team_B`= team6,`Score_B`=0 WHERE `IdHugeMatch`=3 AND `IdMatch`=21;

	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=22;
	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=23;
	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=24;
	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=25;
	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=26;
	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=27;
	UPDATE `HugeMatch` SET `Team_A`= team7,`Score_A`=0,`Team_B`= team8,`Score_B`=0 WHERE `IdHugeMatch`=4 AND `IdMatch`=28;

ELSE	

	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TOURNAMENT IN PROGRESS'; 

END IF;	

	

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Update1SetDisp`(IN `Idm` INT, IN `NumS` ENUM('1','2','3','4','5'), IN `GaA` INT, IN `GaB` INT)
BEGIN
IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	CASE NumS
		WHEN 1 THEN 
			UPDATE `DisplayScores` SET `Set1`=GaA WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo1` FROM `Matchs` WHERE IdMatch=Idm);
			UPDATE `DisplayScores` SET `Set1`=GaB WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo2` FROM `Matchs` WHERE IdMatch=Idm);
		WHEN 2 THEN 
			UPDATE `DisplayScores` SET `Set2`=GaA WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo1` FROM `Matchs` WHERE IdMatch=Idm);
			UPDATE `DisplayScores` SET `Set2`=GaB WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo2` FROM `Matchs` WHERE IdMatch=Idm);
		WHEN 3 THEN 
			UPDATE `DisplayScores` SET `Set3`=GaA WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo1` FROM `Matchs` WHERE IdMatch=Idm);
			UPDATE `DisplayScores` SET `Set3`=GaB WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo2` FROM `Matchs` WHERE IdMatch=Idm);
		WHEN 4 THEN 
			UPDATE `DisplayScores` SET `Set4`=GaA WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo1` FROM `Matchs` WHERE IdMatch=Idm);
			UPDATE `DisplayScores` SET `Set4`=GaB WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo2` FROM `Matchs` WHERE IdMatch=Idm);
		WHEN 5 THEN 
			UPDATE `DisplayScores` SET `Set5`=GaA WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo1` FROM `Matchs` WHERE IdMatch=Idm);
			UPDATE `DisplayScores` SET `Set5`=GaB WHERE IdMatch=idm AND IdOneOrTwo=(SELECT `IdOneOrTwo2` FROM `Matchs` WHERE IdMatch=Idm);
	END CASE;
ELSE 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NON CREE';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateCourt`(IN `IdM` INT, IN `Cou` INT)
    NO SQL
BEGIN
	IF ( (SELECT Statut FROM Matchs WHERE IdMatch = IdM ) = 'PROGRESS' ) THEN 
		IF ( SELECT COUNT(*) FROM `Matchs` WHERE `Court`=Cou AND `Statut`='PROGRESS') = 0 THEN
			UPDATE `Matchs` SET `Court`= Cou WHERE `IdMatch`= IdM;
		ELSE
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'COURT NOT AVAILABLE';
		END IF;
	ELSE
		SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'MATCH NOT IN PROGRESS';
	END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateDisp`(IN `Idm` INT, IN `NumS` ENUM('1','2','3','4','5'), IN `Se1` INT, IN `Se2` INT, IN `Sc1` INT, IN `Sc2` INT, IN `Service` BOOLEAN)
    NO SQL
BEGIN
DECLARE IdOoT1, IdOoT2 INT(11);

IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	SELECT `IdOneOrTwo1` INTO IdOoT1 FROM `Matchs` WHERE IdMatch = Idm;
	SELECT `IdOneOrTwo2` INTO IdOoT2 FROM `Matchs` WHERE IdMatch = Idm;
	CASE NumS
		WHEN 1 THEN 
			
		CASE Service
			WHEN 0 THEN
				UPDATE `DisplayScores` SET `Set1`=Se1,`Score`=sc1,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set1`=Se2,`Score`=sc2,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
			WHEN 1 THEN
				UPDATE `DisplayScores` SET `Set1`=Se1,`Score`=sc1,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set1`=Se2,`Score`=sc2,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
		END CASE;

		WHEN 2 THEN 

		CASE Service
			WHEN 0 THEN
				UPDATE `DisplayScores` SET `Set2`=Se1,`Score`=sc1,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set2`=Se2,`Score`=sc2,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
			WHEN 1 THEN
				UPDATE `DisplayScores` SET `Set2`=Se1,`Score`=sc1,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set2`=Se2,`Score`=sc2,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
		END CASE;

		WHEN 3 THEN 
			
		CASE Service
			WHEN 0 THEN
				UPDATE `DisplayScores` SET `Set3`=Se1,`Score`=sc1,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set3`=Se2,`Score`=sc2,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
			WHEN 1 THEN
				UPDATE `DisplayScores` SET `Set3`=Se1,`Score`=sc1,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set3`=Se2,`Score`=sc2,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
		END CASE;

		WHEN 4 THEN 
			
		CASE Service
			WHEN 0 THEN
				UPDATE `DisplayScores` SET `Set4`=Se1,`Score`=sc1,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set4`=Se2,`Score`=sc2,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
			WHEN 1 THEN
				UPDATE `DisplayScores` SET `Set4`=Se1,`Score`=sc1,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set4`=Se2,`Score`=sc2,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
		END CASE;

		WHEN 5 THEN 
			
		CASE Service
			WHEN 0 THEN
				UPDATE `DisplayScores` SET `Set5`=Se1,`Score`=sc1,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set5`=Se2,`Score`=sc2,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
			WHEN 1 THEN
				UPDATE `DisplayScores` SET `Set5`=Se1,`Score`=sc1,`Service`=0 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT1;
				UPDATE `DisplayScores` SET `Set5`=Se2,`Score`=sc2,`Service`=1 WHERE IdMatch=Idm and IdOneOrTwo=IdOoT2;
		END CASE;

	END CASE;
END IF;
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateStage`(IN `stag` ENUM('','1/4','1/2','Final','SETTING-UP'))
    MODIFIES SQL DATA
BEGIN

DECLARE stat VARCHAR(50);
DECLARE cpt INT(50);

CASE stag

	WHEN 'SETTING-UP' THEN

		SELECT Stage INTO stat FROM Tournament;

		IF(stat = ' ' OR stat = 'SETTING-UP') THEN
			
			UPDATE Tournament SET `Stage` = stag;

		ELSE

			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'HUGEMATCH ALREADY IN PROGRESS'; 

		
		END IF;

	WHEN '1/4' THEN

		SELECT Stage INTO stat FROM Tournament;

		IF(stat = 'SETTING-UP') THEN
			
			UPDATE Tournament SET `Stage` = stag;
			CALL NextHugeMatch('1/4');

		ELSE

			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'HUGEMATCH ALREADY IN PROGRESS'; 

		
		END IF;


	WHEN '1/2' THEN
		SELECT Stage INTO stat FROM Tournament;
		SELECT COUNT(*) INTO cpt FROM Matchs WHERE IdMatch BETWEEN 1 AND 28 AND (Statut = 'FINISH' OR Statut = 'CANCEL' );

		IF(cpt = 28 AND stat='1/4') THEN
			
			UPDATE Tournament SET `Stage` = stag;
			CALL NextHugeMatch('1/2');

		ELSE

			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'HUGEMATCH STILL IN PROGRESS'; 

		
		END IF;

	WHEN 'Final' THEN
		SELECT Stage INTO stat FROM Tournament;
		SELECT COUNT(*) INTO cpt FROM Matchs WHERE IdMatch BETWEEN 29 AND 56 AND (Statut = 'FINISH' OR Statut = 'CANCEL' );

		IF(cpt = 28 AND stat='1/2') THEN
			
			UPDATE Tournament SET `Stage` = stag;
			CALL NextHugeMatch('Final');

		ELSE

			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'HUGEMATCH STILL IN PROGRESS'; 

		
		END IF;

END CASE;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateStatMatch`(IN `IdM` INT)
    NO SQL
BEGIN
	DECLARE NbACE INT;
	DECLARE NbWS INT;
	DECLARE NbWP INT;
	DECLARE NbDF INT;
	DECLARE NbUE INT;
	DECLARE NbFE INT;
	DECLARE NbFsF INT;
	DECLARE NbBreak INT;


	SELECT COUNT(*) INTO NbACE FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Statistic = 'ACE';
	SELECT COUNT(*) INTO NbWS  FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Statistic = 'WS';
	SELECT COUNT(*) INTO NbWP  FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Statistic = 'WP';	
	SELECT COUNT(*) INTO NbDF  FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Statistic = 'DF' ;
	SELECT COUNT(*) INTO NbUE  FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Statistic = 'UE';
	SELECT COUNT(*) INTO NbFE  FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Statistic = 'FE';
	
	SELECT COUNT(*) INTO NbFsF FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND FSF = 1;
	SELECT COUNT(*) INTO NbBreak FROM Log WHERE IdMatch = IdM AND Ref = 'A' AND Break = 1;
	

	UPDATE `StatMatch` SET 
		`ACE`= NbACE,
		`WS`=NbWS,
		`WP`=NbWP,
		`DF`=NbDF,
		`UE`=NbUE,
		`FE`=NbFE,
		`FSF`=NbFsF,
		`Break`=NbBreak
	WHERE `IdMatch`=IdM AND`Ref`='A';
	
	SELECT COUNT(*) INTO NbACE FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Statistic = 'ACE';
	SELECT COUNT(*) INTO NbWS  FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Statistic = 'WS';
	SELECT COUNT(*) INTO NbWP  FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Statistic = 'WP';	
	SELECT COUNT(*) INTO NbDF  FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Statistic = 'DF' ;
	SELECT COUNT(*) INTO NbUE  FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Statistic = 'UE';
	SELECT COUNT(*) INTO NbFE  FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Statistic = 'FE';
	
	SELECT COUNT(*) INTO NbFsF FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND FSF = 1;
	SELECT COUNT(*) INTO NbBreak FROM Log WHERE IdMatch = IdM AND Ref = 'B' AND Break = 1;

	UPDATE `StatMatch` SET 
		`ACE`= NbACE,
		`WS`=NbWS,
		`WP`=NbWP,
		`DF`=NbDF,
		`UE`=NbUE,
		`FE`=NbFE,
		`FSF`=NbFsF,
		`Break`=NbBreak
	WHERE `IdMatch`=IdM AND`Ref`='B';
	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateTeam`(IN `id` INT(11), IN `complet` VARCHAR(50), IN `iso` VARCHAR(4), IN `url` TEXT)
    MODIFIES SQL DATA
BEGIN


IF ((SELECT Stage FROM Tournament) = 'SETTING-UP') THEN

	IF((SELECT COUNT(*) FROM Teams WHERE IdTeam = id) = 1) THEN
	
		UPDATE Teams SET `NameTeam` = iso, `CompleteName` = complet, `URL` = url WHERE IdTeam = id;
	
	 ELSE

   		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'TEAM DOES NOT EXIST'; 

	 END IF;

ELSE 
	
		SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'TOURNAMENT IN PROGRESS'; 
	 
END IF;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateTot`(IN `Idp` INT, IN `Idm` INT, IN `NumS` ENUM('1','2','3','4','5'), IN `SeA` INT, IN `SeB` INT, IN `GaA` INT, IN `GaB` INT, IN `ScA` INT, IN `ScB` INT, IN `serv` BOOLEAN, IN `re` ENUM('A','B'), IN `stat` ENUM('NONE','ACE','WS','WP','DF','UE','FE'), IN `FsF` BOOLEAN, IN `Bre` BOOLEAN)
    NO SQL
BEGIN 

IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	call insertlog(Idp,Idm,SeA,SeB,GaA,GaB,ScA,ScB,serv,re,stat,FsF,Bre);
	call updateDisp(Idm,NumS,GaA,GaB,ScA,ScB,Serv);
	IF (IdP % 10 = 0) THEN
		CALL UpdateStatMatch (IdM);
	END IF;
ELSE 
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'MATCH NOT CREATE';
END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateTournament`(IN `tourn` VARCHAR(50))
    MODIFIES SQL DATA
UPDATE Tournament SET `Name` = tourn$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdHugeMatch`(IN `IdHug` INT(11), IN `idm` INT(11), IN `Win` ENUM('A','B'))
    COMMENT 'Mise a jour table HugeMatch pour un match terminé...'
BEGIN 

DECLARE sc1, sc2,nbMa int;
DECLARE tea VARCHAR(4);
SET nbMa=NbrMatch(IdHug);

	CASE Win
		
		WHEN 'A' THEN
			SELECT  `Team_A` INTO tea FROM `HugeMatch` WHERE `IdHugeMatch`=IdHug AND `IdMatch`=idm;
			UPDATE `Teams` SET `Points`=`Points`+1 WHERE `NameTeam`= tea ;

			UPDATE `HugeMatch` SET `Score_A` = 1 WHERE `IdMatch` = idm AND IdHugeMatch = IdHug; 
			SELECT SUM(Score_A), SUM(Score_B) INTO sc1, sc2 FROM HugeMatch WHERE IdHugeMatch = IdHug;
			
			IF (sc1 + sc2 >= nbMa) THEN
				UPDATE `HugeMatch` SET `HugeStatut` = 'FINISH' WHERE IdHugeMatch = IdHug; 	
			END IF;
		
		WHEN 'B' THEN
			SELECT  `Team_B` INTO tea FROM `HugeMatch` WHERE `IdHugeMatch`=IdHug AND `IdMatch`=idm;
			UPDATE `Teams` SET `Points`=`Points`+1 WHERE `NameTeam`= tea ;
					
			UPDATE `HugeMatch` SET `Score_B` = 1 WHERE `IdMatch` = idm AND IdHugeMatch = IdHug; 	
			SELECT SUM(Score_A), SUM(Score_B) INTO sc1, sc2 FROM HugeMatch WHERE IdHugeMatch = IdHug;
			
			IF (sc1 + sc2 >= nbMa) THEN
				UPDATE `HugeMatch` SET `HugeStatut` = 'FINISH' WHERE IdHugeMatch = IdHug;
			END IF;
		
	END CASE;
					

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdHugeStatut`(IN `IdHug` INT, IN `IdM` INT)
    NO SQL
BEGIN

IF 
	(SELECT  `Team_A` FROM `HugeMatch` WHERE `IdHugeMatch`= IdHug AND `IdMatch`=IdM ) IS NOT NULL
	AND
	(SELECT  `Team_B` FROM `HugeMatch` WHERE `IdHugeMatch`= IdHug AND `IdMatch`=IdM ) IS NOT NULL
THEN
	UPDATE HugeMatch SET HugeStatut = 'PROGRESS' WHERE IdHugeMatch = IdHug;
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UserExists`(IN `usrname` VARCHAR(50))
    NO SQL
BEGIN

SELECT COUNT( * ) as res
FROM Users
WHERE Username = usrname;

END$$

--
-- Fonctions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `IdPlayerToName`(`IdJ` INT) RETURNS varchar(50) CHARSET latin1
    NO SQL
BEGIN
	Declare res varchar(50);
	SELECT CONCAT_WS (' ',`FirstName`, `Name`) INTO res FROM `Players` WHERE `IdPlayer`=IdJ;
	return res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `IdPlayerToRef`(`IdJ` INT, `IdM` INT) RETURNS enum('A','B') CHARSET latin1
    NO SQL
BEGIN
	Declare OoT1 int;
	Declare OoT2 int;
	Declare IdPla int;

	SELECT  `IdOneOrTwo1` INTO OoT1 FROM `Matchs` WHERE `IdMatch`=IdM;
	SELECT  `IdOneOrTwo2` INTO OoT2 FROM `Matchs` WHERE `IdMatch`=IdM;
	
	SELECT `IdPlayer_A` INTO IdPla FROM `OneOrTwo` WHERE `IdOneOrTwo`=OoT1 ;
	IF ( IdPla = IdJ ) THEN 
		RETURN 'A';
	ELSE
		SELECT `IdPlayer_B` INTO IdPla FROM `OneOrTwo` WHERE `IdOneOrTwo`=OoT1 ;
		IF ( IdPla = IdJ ) THEN 
			RETURN 'A';
		ELSE
			SELECT `IdPlayer_A` INTO IdPla FROM `OneOrTwo` WHERE `IdOneOrTwo`=OoT2 ;
			IF ( IdPla = IdJ ) THEN 
				RETURN 'B';
			ELSE
				SELECT `IdPlayer_B` INTO IdPla FROM `OneOrTwo` WHERE `IdOneOrTwo`=OoT2 ;
				IF ( IdPla = IdJ ) THEN 
					RETURN 'B';
				ELSE
					SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'IdJ NOT COMPATIBLE WITH IdM';
				END IF;
			END IF;
		END IF;
	END IF;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `NbrMatch`(`IdHuMatch` INT) RETURNS int(11)
    NO SQL
BEGIN
Declare ret int;
SELECT count(*) into ret FROM `Matchs` WHERE `IdHugeMatch`=IdHuMatch AND `Statut`<>'CANCEL';
return ret;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `TimeMatch`(`Idm` INT) RETURNS time
    NO SQL
BEGIN
DECLARE tmpNow DATETIME;
DECLARE deb DATETIME;
DECLARE fin DATETIME;
DECLARE stat VARCHAR(11);
IF (SELECT COUNT(*) FROM Matchs WHERE IdMatch=Idm )=1 THEN
	SELECT `Statut` INTO stat FROM `Matchs` WHERE `IdMatch` = idm;
	SELECT `DateStart` INTO deb FROM `Matchs` WHERE `IdMatch` = idm;
	IF stat = 'FINISH' THEN
		SELECT `DateEnd` INTO fin FROM `Matchs` WHERE `IdMatch` = idm;
		RETURN subtime(time(fin),time(deb));
	ELSE
		SET tmpNow=Current_TimeStamp;
		RETURN subtime(time(tmpNow),time(deb));
	END IF;
END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `DisplayScores`
--

CREATE TABLE IF NOT EXISTS `DisplayScores` (
  `IdCheck` int(11) NOT NULL AUTO_INCREMENT,
  `IdMatch` int(11) NOT NULL,
  `IdOneOrTwo` int(11) NOT NULL COMMENT 'Numero de l OneOrTwo',
  `Set1` int(11) DEFAULT NULL,
  `Set2` int(11) DEFAULT NULL,
  `Set3` int(11) DEFAULT NULL,
  `Set4` int(11) DEFAULT NULL,
  `Set5` int(11) DEFAULT NULL,
  `Score` varchar(11) DEFAULT NULL COMMENT 'Score du jeu',
  `Service` tinyint(1) DEFAULT NULL,
  `Ref` enum('','A','B') DEFAULT NULL,
  PRIMARY KEY (`IdCheck`,`IdMatch`,`IdOneOrTwo`),
  UNIQUE KEY `IdCheck` (`IdCheck`),
  KEY `IdOneOrTwo` (`IdOneOrTwo`),
  KEY `DisplayScores_ibfk_1` (`IdMatch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=169 ;

--
-- Contenu de la table `DisplayScores`
--

INSERT INTO `DisplayScores` (`IdCheck`, `IdMatch`, `IdOneOrTwo`, `Set1`, `Set2`, `Set3`, `Set4`, `Set5`, `Score`, `Service`, `Ref`) VALUES
(1, 1, 1, 0, -1, -1, -1, -1, '0', 0, 'A'),
(2, 1, 2, 0, -1, -1, -1, -1, '30', 1, 'B'),
(3, 2, 3, 0, -1, -1, -1, -1, '30', 0, 'A'),
(4, 2, 4, 0, -1, -1, -1, -1, '30', 1, 'B'),
(5, 3, 5, 1, -1, -1, -1, -1, '15', 1, 'A'),
(6, 3, 6, 0, -1, -1, -1, -1, '0', 0, 'B'),
(7, 4, 7, 0, -1, -1, -1, -1, '0', 1, 'A'),
(8, 4, 8, 0, -1, -1, -1, -1, '30', 0, 'B'),
(9, 5, 97, 0, -1, -1, -1, -1, '0', 0, 'A'),
(10, 5, 98, 1, -1, -1, -1, -1, '0', 1, 'B'),
(11, 6, 99, 1, -1, -1, -1, -1, '0', 0, 'A'),
(12, 6, 100, 0, -1, -1, -1, -1, '0', 1, 'B'),
(13, 7, 101, 0, -1, -1, -1, -1, '15', 1, 'A'),
(14, 7, 102, 0, -1, -1, -1, -1, '15', 0, 'B'),
(15, 8, 9, 0, -1, -1, -1, -1, '0', 0, 'A'),
(16, 8, 10, 1, -1, -1, -1, -1, '0', 1, 'B'),
(17, 9, 11, 0, -1, -1, -1, -1, '0', 0, 'A'),
(18, 9, 12, 2, -1, -1, -1, -1, '15', 1, 'B'),
(19, 10, 13, 0, -1, -1, -1, -1, '40', 1, 'A'),
(20, 10, 14, 1, -1, -1, -1, -1, '4444', 0, 'B'),
(21, 11, 15, 0, -1, -1, -1, -1, '15', 0, 'A'),
(22, 11, 16, 0, -1, -1, -1, -1, '30', 1, 'B'),
(23, 12, 103, 1, -1, -1, -1, -1, '0', 0, 'A'),
(24, 12, 104, 0, -1, -1, -1, -1, '0', 1, 'B'),
(25, 13, 105, 3, -1, -1, -1, -1, '0', 0, 'A'),
(26, 13, 106, 3, -1, -1, -1, -1, '40', 1, 'B'),
(27, 14, 107, 0, -1, -1, -1, -1, '0', 0, 'A'),
(28, 14, 108, 1, -1, -1, -1, -1, '0', 1, 'B'),
(29, 15, 17, 0, -1, -1, -1, -1, '15', 1, 'A'),
(30, 15, 18, 0, -1, -1, -1, -1, '30', 0, 'B'),
(31, 16, 19, 0, -1, -1, -1, -1, '15', 0, 'A'),
(32, 16, 20, 0, -1, -1, -1, -1, '15', 1, 'B'),
(33, 17, 21, 0, -1, -1, -1, -1, '0', 0, 'A'),
(34, 17, 22, 1, -1, -1, -1, -1, '0', 1, 'B'),
(35, 18, 23, 0, -1, -1, -1, -1, '15', 1, 'A'),
(36, 18, 24, 0, -1, -1, -1, -1, '15', 0, 'B'),
(37, 19, 109, 1, -1, -1, -1, -1, '30', 0, 'A'),
(38, 19, 110, 0, -1, -1, -1, -1, '15', 1, 'B'),
(39, 20, 111, 0, -1, -1, -1, -1, '30', 1, 'A'),
(40, 20, 112, 0, -1, -1, -1, -1, '30', 0, 'B'),
(41, 21, 113, 0, -1, -1, -1, -1, '40', 1, 'A'),
(42, 21, 114, 0, -1, -1, -1, -1, '30', 0, 'B'),
(43, 22, 25, 0, -1, -1, -1, -1, '30', 1, 'A'),
(44, 22, 26, 0, -1, -1, -1, -1, '30', 0, 'B'),
(45, 23, 27, 1, -1, -1, -1, -1, '0', 0, 'A'),
(46, 23, 28, 0, -1, -1, -1, -1, '0', 1, 'B'),
(47, 24, 29, 0, -1, -1, -1, -1, '0', 1, 'A'),
(48, 24, 30, 0, -1, -1, -1, -1, '30', 0, 'B'),
(49, 25, 31, 0, -1, -1, -1, -1, '15', 1, 'A'),
(50, 25, 32, 0, -1, -1, -1, -1, '30', 0, 'B'),
(51, 26, 115, 0, -1, -1, -1, -1, '15', 1, 'A'),
(52, 26, 116, 0, -1, -1, -1, -1, '15', 0, 'B'),
(53, 27, 117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(54, 27, 118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(55, 28, 119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(56, 28, 120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(57, 29, 33, 0, -1, -1, -1, -1, '15', 1, 'A'),
(58, 29, 34, 0, -1, -1, -1, -1, '30', 0, 'B'),
(59, 30, 35, 0, -1, -1, -1, -1, '40', 1, 'A'),
(60, 30, 36, 0, -1, -1, -1, -1, '30', 0, 'B'),
(61, 31, 37, 0, -1, -1, -1, -1, '30', 1, 'A'),
(62, 31, 38, 0, -1, -1, -1, -1, '15', 0, 'B'),
(63, 32, 39, 0, -1, -1, -1, -1, '15', 1, 'A'),
(64, 32, 40, 0, -1, -1, -1, -1, '15', 0, 'B'),
(65, 33, 121, 0, -1, -1, -1, -1, '0', 1, 'A'),
(66, 33, 122, 0, -1, -1, -1, -1, '15', 0, 'B'),
(67, 34, 123, 0, -1, -1, -1, -1, '30', 1, 'A'),
(68, 34, 124, 0, -1, -1, -1, -1, '30', 0, 'B'),
(69, 35, 125, 0, -1, -1, -1, -1, '15', 1, 'A'),
(70, 35, 126, 0, -1, -1, -1, -1, '15', 0, 'B'),
(71, 36, 41, 0, -1, -1, -1, -1, '15', 1, 'A'),
(72, 36, 42, 0, -1, -1, -1, -1, '30', 0, 'B'),
(73, 37, 43, 0, -1, -1, -1, -1, '30', 1, 'A'),
(74, 37, 44, 0, -1, -1, -1, -1, '30', 0, 'B'),
(75, 38, 45, 0, -1, -1, -1, -1, '15', 1, 'A'),
(76, 38, 46, 0, -1, -1, -1, -1, '15', 0, 'B'),
(77, 39, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(78, 39, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(79, 40, 127, 0, -1, -1, -1, -1, '15', 1, 'A'),
(80, 40, 128, 0, -1, -1, -1, -1, '30', 0, 'B'),
(81, 41, 129, 0, -1, -1, -1, -1, '30', 1, 'A'),
(82, 41, 130, 0, -1, -1, -1, -1, '30', 0, 'B'),
(83, 42, 131, 0, -1, -1, -1, -1, '40', 1, 'A'),
(84, 42, 132, 0, -1, -1, -1, -1, '40', 0, 'B'),
(85, 43, 49, 0, -1, -1, -1, -1, '15', 1, 'A'),
(86, 43, 50, 0, -1, -1, -1, -1, '15', 0, 'B'),
(87, 44, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(88, 44, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(89, 45, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(90, 45, 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(91, 46, 55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(92, 46, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(93, 47, 133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(94, 47, 134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(95, 48, 135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(96, 48, 136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(97, 49, 137, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(98, 49, 138, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(99, 50, 57, 0, -1, -1, -1, -1, '30', 1, 'A'),
(100, 50, 58, 0, -1, -1, -1, -1, '30', 0, 'B'),
(101, 51, 59, 0, -1, -1, -1, -1, '15', 1, 'A'),
(102, 51, 60, 0, -1, -1, -1, -1, '30', 0, 'B'),
(103, 52, 61, 0, -1, -1, -1, -1, '15', 1, 'A'),
(104, 52, 62, 0, -1, -1, -1, -1, '30', 0, 'B'),
(105, 53, 63, 0, -1, -1, -1, -1, '0', 1, 'A'),
(106, 53, 64, 0, -1, -1, -1, -1, '30', 0, 'B'),
(107, 54, 139, 0, -1, -1, -1, -1, '30', 1, 'A'),
(108, 54, 140, 0, -1, -1, -1, -1, '30', 0, 'B'),
(109, 55, 141, 0, -1, -1, -1, -1, '15', 1, 'A'),
(110, 55, 142, 0, -1, -1, -1, -1, '15', 0, 'B'),
(111, 56, 143, 0, -1, -1, -1, -1, '15', 1, 'A'),
(112, 56, 144, 0, -1, -1, -1, -1, '15', 0, 'B'),
(113, 57, 65, 0, -1, -1, -1, -1, '30', 1, 'A'),
(114, 57, 66, 0, -1, -1, -1, -1, '30', 0, 'B'),
(115, 58, 67, 6, 6, -1, -1, -1, '4444', 1, 'A'),
(116, 58, 68, 0, 1, -1, -1, -1, '40', 0, 'B'),
(117, 59, 69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(118, 59, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(119, 60, 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(120, 60, 72, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(121, 61, 145, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(122, 61, 146, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(123, 62, 147, 0, -1, -1, -1, -1, '0', 1, 'A'),
(124, 62, 148, 0, -1, -1, -1, -1, '0', 0, 'B'),
(125, 63, 149, 0, -1, -1, -1, -1, '0', 1, 'A'),
(126, 63, 150, 0, -1, -1, -1, -1, '0', 0, 'B'),
(127, 64, 73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(128, 64, 74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(129, 65, 75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(130, 65, 76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(131, 66, 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(132, 66, 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(133, 67, 79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(134, 67, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(135, 68, 151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(136, 68, 152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(137, 69, 153, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(138, 69, 154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(139, 70, 155, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(140, 70, 156, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(141, 71, 81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(142, 71, 82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(143, 72, 83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(144, 72, 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(145, 73, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(146, 73, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(147, 74, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(148, 74, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(149, 75, 157, 2, 2, -1, -1, -1, '0', 0, 'A'),
(150, 75, 158, 6, 6, -1, -1, -1, '40', 1, 'B'),
(151, 76, 159, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(152, 76, 160, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(153, 77, 161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(154, 77, 162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(155, 78, 89, 0, -1, -1, -1, -1, '0', 1, 'A'),
(156, 78, 90, 1, -1, -1, -1, -1, '0', 0, 'B'),
(157, 79, 91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(158, 79, 92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(159, 80, 93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(160, 80, 94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(161, 81, 95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(162, 81, 96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(163, 82, 163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(164, 82, 164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(165, 83, 165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(166, 83, 166, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B'),
(167, 84, 167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A'),
(168, 84, 168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'B');

-- --------------------------------------------------------

--
-- Structure de la table `HugeMatch`
--

CREATE TABLE IF NOT EXISTS `HugeMatch` (
  `IdHugeMatch` int(11) NOT NULL AUTO_INCREMENT,
  `HugeStatut` enum('FINISH','SOON','PROGRESS') NOT NULL,
  `IdMatch` int(11) NOT NULL,
  `Team_A` varchar(20) DEFAULT NULL,
  `Score_A` int(11) DEFAULT '0',
  `Team_B` varchar(20) DEFAULT NULL,
  `Score_B` int(11) DEFAULT '0',
  `Tableau` enum('1/4-P','1/2-P','1/2-5','Final-P','Final-3','Final-5','Final-7') NOT NULL COMMENT 'Type de tableau du match soit (Tab Principal, Tab 3ème place, Tab 5ème place, Tab 7ème place)',
  PRIMARY KEY (`IdHugeMatch`,`IdMatch`),
  KEY `IdMatch` (`IdMatch`),
  KEY `IdHugeMatch` (`IdHugeMatch`),
  KEY `Team_A` (`Team_A`),
  KEY `Team_B` (`Team_B`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `HugeMatch`
--

INSERT INTO `HugeMatch` (`IdHugeMatch`, `HugeStatut`, `IdMatch`, `Team_A`, `Score_A`, `Team_B`, `Score_B`, `Tableau`) VALUES
(1, 'FINISH', 1, 'BEL', 1, 'CHN', 0, '1/4-P'),
(1, 'FINISH', 2, 'BEL', 1, 'CHN', 0, '1/4-P'),
(1, 'FINISH', 3, 'BEL', 1, 'CHN', 0, '1/4-P'),
(1, 'FINISH', 4, 'BEL', 0, 'CHN', 1, '1/4-P'),
(1, 'FINISH', 5, 'BEL', 1, 'CHN', 0, '1/4-P'),
(1, 'FINISH', 6, 'BEL', 0, 'CHN', 1, '1/4-P'),
(1, 'FINISH', 7, 'BEL', 0, 'CHN', 1, '1/4-P'),
(2, 'FINISH', 8, 'DEU', 1, 'FRA', 0, '1/4-P'),
(2, 'FINISH', 9, 'DEU', 1, 'FRA', 0, '1/4-P'),
(2, 'FINISH', 10, 'DEU', 0, 'FRA', 1, '1/4-P'),
(2, 'FINISH', 11, 'DEU', 0, 'FRA', 1, '1/4-P'),
(2, 'FINISH', 12, 'DEU', 1, 'FRA', 0, '1/4-P'),
(2, 'FINISH', 13, 'DEU', 1, 'FRA', 0, '1/4-P'),
(2, 'FINISH', 14, 'DEU', 1, 'FRA', 0, '1/4-P'),
(3, 'FINISH', 15, 'GBR', 1, 'IRL', 0, '1/4-P'),
(3, 'FINISH', 16, 'GBR', 1, 'IRL', 0, '1/4-P'),
(3, 'FINISH', 17, 'GBR', 0, 'IRL', 1, '1/4-P'),
(3, 'FINISH', 18, 'GBR', 0, 'IRL', 1, '1/4-P'),
(3, 'FINISH', 19, 'GBR', 0, 'IRL', 1, '1/4-P'),
(3, 'FINISH', 20, 'GBR', 1, 'IRL', 0, '1/4-P'),
(3, 'FINISH', 21, 'GBR', 1, 'IRL', 0, '1/4-P'),
(4, 'FINISH', 22, 'RUS', 1, 'USA', 0, '1/4-P'),
(4, 'FINISH', 23, 'RUS', 0, 'USA', 1, '1/4-P'),
(4, 'FINISH', 24, 'RUS', 1, 'USA', 0, '1/4-P'),
(4, 'FINISH', 25, 'RUS', 1, 'USA', 0, '1/4-P'),
(4, 'FINISH', 26, 'RUS', 1, 'USA', 0, '1/4-P'),
(4, 'FINISH', 27, 'RUS', 0, 'USA', 0, '1/4-P'),
(4, 'FINISH', 28, 'RUS', 0, 'USA', 0, '1/4-P'),
(5, 'FINISH', 29, 'BEL', 1, 'DEU', 0, '1/2-P'),
(5, 'FINISH', 30, 'BEL', 1, 'DEU', 0, '1/2-P'),
(5, 'FINISH', 31, 'BEL', 1, 'DEU', 0, '1/2-P'),
(5, 'FINISH', 32, 'BEL', 1, 'DEU', 0, '1/2-P'),
(5, 'FINISH', 33, 'BEL', 1, 'DEU', 0, '1/2-P'),
(5, 'FINISH', 34, 'BEL', 1, 'DEU', 0, '1/2-P'),
(5, 'FINISH', 35, 'BEL', 1, 'DEU', 0, '1/2-P'),
(6, 'FINISH', 36, 'GBR', 1, 'RUS', 0, '1/2-P'),
(6, 'FINISH', 37, 'GBR', 1, 'RUS', 0, '1/2-P'),
(6, 'FINISH', 38, 'GBR', 1, 'RUS', 0, '1/2-P'),
(6, 'FINISH', 39, 'GBR', 0, 'RUS', 0, '1/2-P'),
(6, 'FINISH', 40, 'GBR', 1, 'RUS', 0, '1/2-P'),
(6, 'FINISH', 41, 'GBR', 1, 'RUS', 0, '1/2-P'),
(6, 'FINISH', 42, 'GBR', 0, 'RUS', 1, '1/2-P'),
(7, 'FINISH', 43, 'CHN', 1, 'FRA', 0, '1/2-5'),
(7, 'FINISH', 44, 'CHN', 0, 'FRA', 0, '1/2-5'),
(7, 'FINISH', 45, 'CHN', 0, 'FRA', 0, '1/2-5'),
(7, 'FINISH', 46, 'CHN', 0, 'FRA', 0, '1/2-5'),
(7, 'FINISH', 47, 'CHN', 0, 'FRA', 0, '1/2-5'),
(7, 'FINISH', 48, 'CHN', 0, 'FRA', 0, '1/2-5'),
(7, 'FINISH', 49, 'CHN', 0, 'FRA', 0, '1/2-5'),
(8, 'FINISH', 50, 'IRL', 1, 'USA', 0, '1/2-5'),
(8, 'FINISH', 51, 'IRL', 1, 'USA', 0, '1/2-5'),
(8, 'FINISH', 52, 'IRL', 1, 'USA', 0, '1/2-5'),
(8, 'FINISH', 53, 'IRL', 1, 'USA', 0, '1/2-5'),
(8, 'FINISH', 54, 'IRL', 1, 'USA', 0, '1/2-5'),
(8, 'FINISH', 55, 'IRL', 1, 'USA', 0, '1/2-5'),
(8, 'FINISH', 56, 'IRL', 1, 'USA', 0, '1/2-5'),
(9, 'PROGRESS', 57, 'BEL', 1, 'GBR', 0, 'Final-P'),
(9, 'PROGRESS', 58, 'BEL', 1, 'GBR', 0, 'Final-P'),
(9, 'PROGRESS', 59, 'BEL', 0, 'GBR', 0, 'Final-P'),
(9, 'PROGRESS', 60, 'BEL', 0, 'GBR', 0, 'Final-P'),
(9, 'PROGRESS', 61, 'BEL', 0, 'GBR', 0, 'Final-P'),
(9, 'PROGRESS', 62, 'BEL', 1, 'GBR', 0, 'Final-P'),
(9, 'PROGRESS', 63, 'BEL', 1, 'GBR', 0, 'Final-P'),
(10, 'PROGRESS', 64, 'DEU', 0, 'RUS', 0, 'Final-3'),
(10, 'PROGRESS', 65, 'DEU', 0, 'RUS', 0, 'Final-3'),
(10, 'PROGRESS', 66, 'DEU', 0, 'RUS', 0, 'Final-3'),
(10, 'PROGRESS', 67, 'DEU', 0, 'RUS', 0, 'Final-3'),
(10, 'PROGRESS', 68, 'DEU', 0, 'RUS', 0, 'Final-3'),
(10, 'PROGRESS', 69, 'DEU', 0, 'RUS', 0, 'Final-3'),
(10, 'PROGRESS', 70, 'DEU', 0, 'RUS', 0, 'Final-3'),
(11, 'PROGRESS', 71, 'CHN', 0, 'IRL', 0, 'Final-5'),
(11, 'PROGRESS', 72, 'CHN', 0, 'IRL', 0, 'Final-5'),
(11, 'PROGRESS', 73, 'CHN', 0, 'IRL', 0, 'Final-5'),
(11, 'PROGRESS', 74, 'CHN', 0, 'IRL', 0, 'Final-5'),
(11, 'PROGRESS', 75, 'CHN', 0, 'IRL', 1, 'Final-5'),
(11, 'PROGRESS', 76, 'CHN', 0, 'IRL', 0, 'Final-5'),
(11, 'PROGRESS', 77, 'CHN', 0, 'IRL', 0, 'Final-5'),
(12, 'PROGRESS', 78, 'FRA', 0, 'USA', 0, 'Final-7'),
(12, 'PROGRESS', 79, 'FRA', 0, 'USA', 0, 'Final-7'),
(12, 'PROGRESS', 80, 'FRA', 0, 'USA', 0, 'Final-7'),
(12, 'PROGRESS', 81, 'FRA', 0, 'USA', 0, 'Final-7'),
(12, 'PROGRESS', 82, 'FRA', 0, 'USA', 0, 'Final-7'),
(12, 'PROGRESS', 83, 'FRA', 0, 'USA', 0, 'Final-7'),
(12, 'PROGRESS', 84, 'FRA', 0, 'USA', 0, 'Final-7');

-- --------------------------------------------------------

--
-- Structure de la table `Log`
--

CREATE TABLE IF NOT EXISTS `Log` (
  `Id` int(11) NOT NULL,
  `IdMatch` int(11) NOT NULL,
  `SetA` int(11) DEFAULT NULL,
  `SetB` int(11) DEFAULT NULL,
  `GameA` int(11) DEFAULT NULL,
  `GameB` int(11) DEFAULT NULL,
  `ScoreA` int(11) DEFAULT NULL,
  `ScoreB` int(11) DEFAULT NULL,
  `Service` tinyint(1) DEFAULT NULL,
  `Ref` enum('A','B') DEFAULT NULL,
  `Statistic` enum('NONE','ACE','WS','WP','DF','UE','FE') DEFAULT NULL COMMENT 'NONE : pas de stats particulières | WS :Winner Serve | WP : Winner Point | DF : Double Fault | UE :Unforced Error | FE : Forced Error',
  `FSF` tinyint(1) DEFAULT NULL COMMENT 'First Serve Fault',
  `Break` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`,`IdMatch`),
  KEY `IdMatch` (`IdMatch`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Log`
--

INSERT INTO `Log` (`Id`, `IdMatch`, `SetA`, `SetB`, `GameA`, `GameB`, `ScoreA`, `ScoreB`, `Service`, `Ref`, `Statistic`, `FSF`, `Break`) VALUES
(0, 1, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 2, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 3, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 4, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 5, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 6, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 7, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 8, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 9, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 10, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 11, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 12, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 13, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 14, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 15, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 16, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(0, 17, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 18, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 19, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 20, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 21, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 22, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 23, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 24, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 25, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 26, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 29, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 30, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 31, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 32, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 33, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 34, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 35, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 36, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 37, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 38, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 40, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 41, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 42, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 43, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 50, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 51, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 52, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 53, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 54, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 55, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 56, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 57, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 58, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 62, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 63, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 75, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL),
(0, 78, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, NULL, NULL),
(1, 1, 0, 0, 0, 0, 0, 15, 1, 'B', 'FE', 0, 0),
(1, 2, 0, 0, 0, 0, 15, 0, 1, 'A', 'FE', 1, 0),
(1, 3, 0, 0, 0, 0, 15, 0, 1, 'A', 'FE', 0, 0),
(1, 4, 0, 0, 0, 0, 0, 15, 0, 'B', 'FE', 0, 0),
(1, 5, 0, 0, 0, 0, 0, 15, 0, 'B', 'FE', 0, 0),
(1, 6, 0, 0, 0, 0, 0, 15, 0, 'B', 'FE', 0, 0),
(1, 7, 0, 0, 0, 0, 15, 0, 0, 'A', 'FE', 0, 0),
(1, 8, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 9, 0, 0, 0, 0, 0, 15, 1, 'B', 'FE', 0, 0),
(1, 10, 0, 0, 0, 0, 0, 15, 1, 'B', 'FE', 0, 0),
(1, 11, 0, 0, 0, 0, 15, 0, 1, 'A', 'FE', 0, 0),
(1, 12, 0, 0, 0, 0, 15, 0, 0, 'A', 'FE', 0, 0),
(1, 13, 0, 0, 0, 0, 15, 0, 1, 'A', 'FE', 0, 0),
(1, 14, 0, 0, 0, 0, 0, 15, 0, 'B', 'FE', 0, 0),
(1, 15, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 16, 0, 0, 0, 0, 0, 15, 1, 'B', 'FE', 0, 0),
(1, 17, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 18, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 19, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 20, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 21, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 22, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 23, 0, 0, 0, 0, 15, 0, 0, 'A', 'UE', 0, 0),
(1, 24, 0, 0, 0, 0, 0, 15, 0, 'B', 'FE', 0, 0),
(1, 25, 0, 0, 0, 0, 0, 15, 0, 'B', 'ACE', 0, 0),
(1, 26, 0, 0, 0, 0, 15, 0, 0, 'A', 'UE', 0, 0),
(1, 29, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 30, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 31, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 32, 0, 0, 0, 0, 15, 0, 0, 'A', 'UE', 0, 0),
(1, 33, 0, 0, 0, 0, 0, 15, 0, 'B', 'UE', 0, 0),
(1, 34, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 35, 0, 0, 0, 0, 0, 15, 0, 'B', 'UE', 0, 0),
(1, 36, 0, 0, 0, 0, 0, 15, 0, 'B', 'ACE', 0, 0),
(1, 37, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 38, 0, 0, 0, 0, 15, 0, 0, 'A', 'UE', 0, 0),
(1, 40, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 41, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 42, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 43, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 50, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 51, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 52, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 53, 0, 0, 0, 0, 0, 15, 0, 'B', 'ACE', 0, 0),
(1, 54, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 55, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 56, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 57, 0, 0, 0, 0, 0, 15, 0, 'B', 'UE', 0, 0),
(1, 58, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 75, 0, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(1, 78, 0, 0, 0, 0, 0, 15, 1, 'B', 'ACE', 0, 0),
(2, 1, 0, 0, 0, 0, 0, 30, 1, 'B', 'FE', 0, 0),
(2, 2, 0, 0, 0, 0, 30, 0, 1, 'A', 'ACE', 0, 0),
(2, 3, 0, 0, 0, 0, 30, 0, 1, 'A', 'ACE', 0, 0),
(2, 4, 0, 0, 0, 0, 0, 30, 0, 'B', 'FE', 0, 0),
(2, 5, 0, 0, 0, 0, 0, 30, 0, 'B', 'FE', 0, 0),
(2, 6, 0, 0, 0, 0, 15, 15, 0, 'A', 'FE', 0, 0),
(2, 7, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 8, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 9, 0, 0, 0, 0, 0, 30, 1, 'B', 'FE', 0, 0),
(2, 10, 0, 0, 0, 0, 0, 30, 1, 'B', 'FE', 0, 0),
(2, 11, 0, 0, 0, 0, 15, 15, 1, 'B', 'FE', 0, 0),
(2, 12, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 13, 0, 0, 0, 0, 30, 0, 1, 'A', 'FE', 0, 0),
(2, 14, 0, 0, 0, 0, 0, 30, 0, 'B', 'FE', 0, 0),
(2, 15, 0, 0, 0, 0, 15, 15, 0, 'B', 'UE', 0, 0),
(2, 16, 0, 0, 0, 0, 15, 15, 1, 'A', 'FE', 0, 0),
(2, 17, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 18, 0, 0, 0, 0, 15, 15, 0, 'B', 'UE', 0, 0),
(2, 19, 0, 0, 0, 0, 30, 0, 0, 'A', 'FE', 0, 0),
(2, 20, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 21, 0, 0, 0, 0, 30, 0, 0, 'A', 'WP', 0, 0),
(2, 22, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 23, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 24, 0, 0, 0, 0, 0, 30, 0, 'B', 'UE', 0, 0),
(2, 25, 0, 0, 0, 0, 0, 30, 0, 'B', 'FE', 0, 0),
(2, 26, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 29, 0, 0, 0, 0, 15, 15, 0, 'B', 'WP', 0, 0),
(2, 30, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 31, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 32, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 34, 0, 0, 0, 0, 15, 15, 0, 'B', 'UE', 0, 0),
(2, 35, 0, 0, 0, 0, 15, 15, 0, 'A', 'UE', 0, 0),
(2, 36, 0, 0, 0, 0, 15, 15, 0, 'A', 'ACE', 0, 0),
(2, 37, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 38, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 40, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 41, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 42, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 43, 0, 0, 0, 0, 15, 15, 0, 'B', 'FE', 0, 0),
(2, 50, 0, 0, 0, 0, 15, 15, 0, 'B', 'UE', 0, 0),
(2, 51, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 52, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 53, 0, 0, 0, 0, 0, 30, 0, 'B', 'FE', 0, 0),
(2, 54, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 55, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 56, 0, 0, 0, 0, 15, 15, 0, 'B', 'ACE', 0, 0),
(2, 57, 0, 0, 0, 0, 15, 15, 0, 'A', 'FE', 0, 0),
(2, 58, 0, 0, 0, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(2, 75, 0, 0, 0, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(2, 78, 0, 0, 0, 0, 0, 30, 1, 'B', 'ACE', 0, 0),
(3, 2, 0, 0, 0, 0, 30, 15, 1, 'B', 'FE', 0, 0),
(3, 3, 0, 0, 0, 0, 40, 0, 1, 'A', 'UE', 0, 1),
(3, 5, 0, 0, 0, 0, 0, 40, 0, 'B', 'FE', 0, 1),
(3, 6, 0, 0, 0, 0, 30, 15, 0, 'A', 'FE', 0, 0),
(3, 8, 0, 0, 0, 0, 15, 30, 0, 'B', 'ACE', 0, 0),
(3, 9, 0, 0, 0, 0, 0, 40, 1, 'B', 'FE', 0, 0),
(3, 10, 0, 0, 0, 0, 0, 40, 1, 'B', 'FE', 0, 0),
(3, 11, 0, 0, 0, 0, 15, 30, 1, 'B', 'FE', 0, 0),
(3, 12, 0, 0, 0, 0, 30, 15, 0, 'A', 'ACE', 0, 0),
(3, 13, 0, 0, 0, 0, 40, 0, 1, 'A', 'FE', 0, 1),
(3, 14, 0, 0, 0, 0, 0, 40, 0, 'B', 'FE', 0, 1),
(3, 15, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 17, 0, 0, 0, 0, 30, 15, 0, 'A', 'ACE', 0, 0),
(3, 19, 0, 0, 0, 0, 30, 15, 0, 'B', 'FE', 0, 0),
(3, 20, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 21, 0, 0, 0, 0, 30, 15, 0, 'B', 'FE', 0, 0),
(3, 22, 0, 0, 0, 0, 30, 15, 0, 'A', 'WP', 0, 0),
(3, 23, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 25, 0, 0, 0, 0, 15, 30, 0, 'A', 'UE', 0, 0),
(3, 29, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 30, 0, 0, 0, 0, 15, 30, 0, 'B', 'WP', 0, 0),
(3, 31, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 34, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 36, 0, 0, 0, 0, 15, 30, 0, 'B', 'WP', 0, 0),
(3, 37, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 40, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 41, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 42, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 50, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 51, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 52, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 54, 0, 0, 0, 0, 30, 15, 0, 'A', 'UE', 0, 0),
(3, 57, 0, 0, 0, 0, 15, 30, 0, 'B', 'FE', 0, 0),
(3, 58, 0, 0, 0, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(3, 75, 0, 0, 0, 0, 40, 0, 0, 'A', 'FE', 0, 0),
(3, 78, 0, 0, 0, 0, 0, 40, 1, 'B', 'ACE', 0, 0),
(4, 2, 0, 0, 0, 0, 30, 30, 1, 'B', 'FE', 0, 0),
(4, 3, 0, 0, 1, 0, 0, 0, 0, 'A', 'FE', 1, 0),
(4, 5, 0, 0, 0, 1, 0, 0, 1, 'B', 'FE', 0, 0),
(4, 6, 0, 0, 0, 0, 40, 15, 0, 'A', 'FE', 0, 0),
(4, 8, 0, 0, 0, 0, 15, 40, 0, 'B', 'WS', 0, 1),
(4, 9, 0, 0, 0, 1, 0, 0, 0, 'B', 'FE', 0, 0),
(4, 10, 0, 0, 0, 1, 0, 0, 0, 'B', 'FE', 0, 0),
(4, 12, 0, 0, 0, 0, 30, 30, 0, 'B', 'FE', 0, 0),
(4, 13, 0, 0, 1, 0, 0, 0, 0, 'A', 'FE', 0, 0),
(4, 14, 0, 0, 0, 1, 0, 0, 1, 'B', 'FE', 0, 0),
(4, 17, 0, 0, 0, 0, 30, 30, 0, 'B', 'UE', 0, 0),
(4, 19, 0, 0, 0, 0, 40, 15, 0, 'A', 'ACE', 0, 0),
(4, 20, 0, 0, 0, 0, 30, 30, 0, 'B', 'WP', 0, 0),
(4, 21, 0, 0, 0, 0, 30, 30, 0, 'B', 'UE', 0, 0),
(4, 22, 0, 0, 0, 0, 30, 30, 0, 'B', 'UE', 0, 0),
(4, 23, 0, 0, 0, 0, 30, 30, 0, 'B', 'FE', 0, 0),
(4, 30, 0, 0, 0, 0, 30, 30, 0, 'A', 'UE', 0, 0),
(4, 34, 0, 0, 0, 0, 30, 30, 0, 'B', 'FE', 0, 0),
(4, 37, 0, 0, 0, 0, 30, 30, 0, 'A', 'FE', 0, 0),
(4, 41, 0, 0, 0, 0, 30, 30, 0, 'B', 'UE', 0, 0),
(4, 42, 0, 0, 0, 0, 40, 15, 0, 'A', 'FE', 0, 0),
(4, 50, 0, 0, 0, 0, 30, 30, 0, 'B', 'FE', 0, 0),
(4, 54, 0, 0, 0, 0, 30, 30, 0, 'B', 'FE', 0, 0),
(4, 57, 0, 0, 0, 0, 30, 30, 0, 'A', 'UE', 0, 0),
(4, 58, 0, 0, 1, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(4, 75, 0, 0, 0, 0, 40, 15, 0, 'B', 'FE', 0, 0),
(4, 78, 0, 0, 0, 1, 0, 0, 0, 'B', 'ACE', 0, 0),
(5, 3, 0, 0, 1, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(5, 6, 0, 0, 1, 0, 0, 0, 1, 'A', 'FE', 0, 0),
(5, 8, 0, 0, 0, 1, 0, 0, 1, 'B', 'ACE', 0, 0),
(5, 9, 0, 0, 0, 1, 0, 15, 0, 'B', 'ACE', 0, 0),
(5, 10, 0, 0, 0, 1, 0, 15, 0, 'B', 'FE', 0, 0),
(5, 12, 0, 0, 0, 0, 40, 30, 0, 'A', 'UE', 0, 0),
(5, 13, 0, 0, 1, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(5, 17, 0, 0, 0, 0, 30, 40, 0, 'B', 'WP', 0, 1),
(5, 19, 0, 0, 0, 0, 40, 30, 0, 'B', 'UE', 0, 0),
(5, 21, 0, 0, 0, 0, 40, 30, 0, 'A', 'ACE', 0, 0),
(5, 23, 0, 0, 0, 0, 40, 30, 0, 'A', 'FE', 0, 0),
(5, 30, 0, 0, 0, 0, 40, 30, 0, 'A', 'ACE', 0, 0),
(5, 42, 0, 0, 0, 0, 40, 30, 0, 'B', 'UE', 0, 0),
(5, 58, 0, 0, 1, 0, 15, 0, 1, 'A', 'ACE', 0, 0),
(5, 75, 0, 0, 0, 0, 40, 30, 0, 'B', 'FE', 0, 0),
(6, 9, 0, 0, 0, 1, 0, 30, 0, 'B', 'ACE', 0, 0),
(6, 10, 0, 0, 0, 1, 0, 30, 0, 'B', 'ACE', 0, 0),
(6, 12, 0, 0, 0, 0, 40, 40, 0, 'B', 'UE', 0, 0),
(6, 13, 0, 0, 1, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(6, 17, 0, 0, 0, 0, 40, 40, 0, 'A', 'WP', 0, 0),
(6, 19, 0, 0, 1, 0, 0, 0, 1, 'A', 'UE', 0, 0),
(6, 23, 0, 0, 1, 0, 0, 0, 1, 'A', 'UE', 0, 0),
(6, 42, 0, 0, 0, 0, 40, 40, 0, 'B', 'FE', 0, 0),
(6, 58, 0, 0, 1, 0, 30, 0, 1, 'A', 'ACE', 0, 0),
(6, 75, 0, 0, 1, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(7, 9, 0, 0, 0, 1, 0, 40, 0, 'B', 'ACE', 0, 1),
(7, 10, 0, 0, 0, 1, 0, 40, 0, 'B', 'ACE', 0, 1),
(7, 12, 0, 0, 1, 0, 0, 0, 1, 'A', 'FE', 0, 0),
(7, 13, 0, 0, 1, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(7, 17, 0, 0, 0, 0, 40, 4444, 0, 'B', 'ACE', 0, 1),
(7, 19, 0, 0, 1, 0, 15, 0, 1, 'A', 'ACE', 0, 0),
(7, 58, 0, 0, 1, 0, 40, 0, 1, 'A', 'ACE', 0, 1),
(7, 75, 0, 0, 1, 0, 0, 15, 1, 'B', 'ACE', 0, 0),
(8, 9, 0, 0, 0, 2, 0, 0, 1, 'B', 'ACE', 0, 0),
(8, 10, 0, 0, 0, 1, 15, 40, 0, 'A', 'FE', 0, 1),
(8, 13, 0, 0, 2, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(8, 17, 0, 0, 0, 0, 40, 40, 0, 'A', 'ACE', 0, 0),
(8, 19, 0, 0, 1, 0, 15, 15, 1, 'B', 'UE', 0, 0),
(8, 58, 0, 0, 2, 0, 0, 0, 0, 'A', 'ACE', 0, 0),
(8, 75, 0, 0, 1, 0, 0, 30, 1, 'B', 'FE', 0, 0),
(9, 9, 0, 0, 0, 2, 0, 15, 1, 'B', 'ACE', 0, 0),
(9, 10, 0, 0, 0, 1, 30, 40, 0, 'A', 'FE', 0, 1),
(9, 13, 0, 0, 2, 0, 15, 0, 1, 'A', 'ACE', 0, 0),
(9, 17, 0, 0, 0, 0, 40, 4444, 0, 'B', 'WP', 0, 1),
(9, 19, 0, 0, 1, 0, 30, 15, 1, 'A', 'FE', 0, 0),
(9, 58, 0, 0, 2, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(9, 75, 0, 0, 1, 0, 15, 30, 1, 'A', 'FE', 0, 0),
(10, 10, 0, 0, 0, 1, 40, 40, 0, 'A', 'FE', 0, 0),
(10, 13, 0, 0, 2, 0, 30, 0, 1, 'A', 'ACE', 0, 0),
(10, 17, 0, 0, 0, 1, 0, 0, 1, 'B', 'FE', 0, 0),
(10, 58, 0, 0, 2, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(10, 75, 0, 0, 1, 0, 15, 40, 1, 'B', 'ACE', 0, 0),
(11, 10, 0, 0, 0, 1, 40, 4444, 0, 'B', 'ACE', 0, 1),
(11, 13, 0, 0, 2, 0, 40, 0, 1, 'A', 'ACE', 0, 1),
(11, 58, 0, 0, 2, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(11, 75, 0, 0, 1, 0, 30, 40, 1, 'A', 'FE', 0, 0),
(12, 13, 0, 0, 3, 0, 0, 0, 0, 'A', 'ACE', 0, 0),
(12, 58, 0, 0, 3, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(12, 75, 0, 0, 1, 1, 0, 0, 0, 'B', 'ACE', 0, 0),
(13, 13, 0, 0, 3, 0, 0, 15, 0, 'B', 'FE', 0, 0),
(13, 58, 0, 0, 3, 0, 15, 0, 1, 'A', 'WS', 0, 0),
(13, 75, 0, 0, 1, 1, 0, 15, 0, 'B', 'ACE', 0, 0),
(14, 13, 0, 0, 3, 0, 0, 30, 0, 'B', 'FE', 0, 0),
(14, 58, 0, 0, 3, 0, 30, 0, 1, 'A', 'WS', 0, 0),
(14, 75, 0, 0, 1, 1, 0, 30, 0, 'B', 'ACE', 0, 0),
(15, 13, 0, 0, 3, 0, 0, 40, 0, 'B', 'FE', 0, 1),
(15, 58, 0, 0, 3, 0, 40, 0, 1, 'A', 'WS', 0, 1),
(15, 75, 0, 0, 1, 1, 0, 40, 0, 'B', 'ACE', 0, 1),
(16, 13, 0, 0, 3, 1, 0, 0, 1, 'B', 'FE', 0, 0),
(16, 58, 0, 0, 3, 0, 40, 15, 1, 'B', 'WP', 0, 1),
(16, 75, 0, 0, 1, 2, 0, 0, 1, 'B', 'ACE', 0, 0),
(17, 13, 0, 0, 3, 1, 0, 15, 1, 'B', 'FE', 0, 0),
(17, 58, 0, 0, 4, 0, 0, 0, 0, 'A', 'ACE', 0, 0),
(17, 75, 0, 0, 1, 2, 0, 15, 1, 'B', 'ACE', 0, 0),
(18, 13, 0, 0, 3, 1, 0, 30, 1, 'B', 'FE', 0, 0),
(18, 58, 0, 0, 4, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(18, 75, 0, 0, 1, 2, 0, 30, 1, 'B', 'ACE', 0, 0),
(19, 13, 0, 0, 3, 1, 0, 40, 1, 'B', 'FE', 0, 0),
(19, 58, 0, 0, 4, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(19, 75, 0, 0, 1, 2, 0, 40, 1, 'B', 'ACE', 0, 0),
(20, 13, 0, 0, 3, 2, 0, 0, 0, 'B', 'FE', 0, 0),
(20, 58, 0, 0, 4, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(20, 75, 0, 0, 1, 3, 0, 0, 0, 'B', 'ACE', 0, 0),
(21, 13, 0, 0, 3, 2, 0, 15, 0, 'B', 'FE', 0, 0),
(21, 58, 0, 0, 5, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(21, 75, 0, 0, 1, 3, 0, 15, 0, 'B', 'ACE', 0, 0),
(22, 13, 0, 0, 3, 2, 0, 30, 0, 'B', 'FE', 0, 0),
(22, 58, 0, 0, 5, 0, 0, 15, 1, 'B', 'ACE', 0, 0),
(22, 75, 0, 0, 1, 3, 0, 30, 0, 'B', 'ACE', 0, 0),
(23, 13, 0, 0, 3, 2, 0, 40, 0, 'B', 'FE', 0, 1),
(23, 58, 0, 0, 5, 0, 0, 30, 1, 'B', 'ACE', 0, 0),
(23, 75, 0, 0, 1, 3, 0, 40, 0, 'B', 'ACE', 0, 1),
(24, 13, 0, 0, 3, 3, 0, 0, 1, 'B', 'FE', 0, 0),
(24, 58, 0, 0, 5, 0, 0, 40, 1, 'B', 'ACE', 0, 0),
(24, 75, 0, 0, 1, 4, 0, 0, 1, 'B', 'WS', 0, 0),
(25, 13, 0, 0, 3, 3, 0, 15, 1, 'B', 'FE', 0, 0),
(25, 58, 0, 0, 5, 0, 15, 40, 1, 'A', 'ACE', 0, 0),
(25, 75, 0, 0, 1, 4, 15, 0, 1, 'A', 'ACE', 0, 0),
(26, 13, 0, 0, 3, 3, 0, 30, 1, 'B', 'FE', 0, 0),
(26, 58, 0, 0, 5, 0, 30, 40, 1, 'A', 'ACE', 0, 0),
(26, 75, 0, 0, 1, 4, 30, 0, 1, 'A', 'FE', 0, 0),
(27, 13, 0, 0, 3, 3, 0, 40, 1, 'B', 'FE', 0, 0),
(27, 58, 0, 0, 5, 0, 40, 40, 1, 'A', 'ACE', 0, 0),
(27, 75, 0, 0, 1, 4, 40, 0, 1, 'A', 'ACE', 0, 1),
(28, 58, 0, 0, 5, 0, 4444, 40, 1, 'A', 'ACE', 0, 1),
(28, 75, 0, 0, 2, 4, 0, 0, 0, 'A', 'FE', 0, 0),
(29, 58, 1, 0, 0, 0, 0, 0, 0, 'A', 'ACE', 0, 0),
(29, 75, 0, 0, 2, 4, 0, 15, 0, 'B', 'ACE', 0, 0),
(30, 58, 1, 0, 0, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(30, 75, 0, 0, 2, 4, 0, 30, 0, 'B', 'ACE', 0, 0),
(31, 58, 1, 0, 0, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(31, 75, 0, 0, 2, 4, 0, 40, 0, 'B', 'WS', 0, 1),
(32, 58, 1, 0, 0, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(32, 75, 0, 0, 2, 5, 0, 0, 1, 'B', 'ACE', 0, 0),
(33, 58, 1, 0, 1, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(33, 75, 0, 0, 2, 5, 0, 15, 1, 'B', 'ACE', 0, 0),
(34, 58, 1, 0, 1, 0, 15, 0, 1, 'A', 'ACE', 0, 0),
(34, 75, 0, 0, 2, 5, 0, 30, 1, 'B', 'FE', 0, 0),
(35, 58, 1, 0, 1, 0, 30, 0, 1, 'A', 'ACE', 0, 0),
(35, 75, 0, 0, 2, 5, 0, 40, 1, 'B', 'ACE', 0, 0),
(36, 58, 1, 0, 1, 0, 40, 0, 1, 'A', 'ACE', 0, 1),
(36, 75, 0, 1, 0, 0, 0, 0, 0, 'B', 'FE', 0, 0),
(37, 58, 1, 0, 2, 0, 0, 0, 0, 'A', 'ACE', 0, 0),
(37, 75, 0, 1, 0, 0, 0, 15, 0, 'B', 'ACE', 0, 0),
(38, 58, 1, 0, 2, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(38, 75, 0, 1, 0, 0, 0, 30, 0, 'B', 'ACE', 0, 0),
(39, 58, 1, 0, 2, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(39, 75, 0, 1, 0, 0, 0, 40, 0, 'B', 'FE', 0, 1),
(40, 58, 1, 0, 2, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(40, 75, 0, 1, 0, 1, 0, 0, 1, 'B', 'ACE', 0, 0),
(41, 58, 1, 0, 3, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(41, 75, 0, 1, 0, 1, 15, 0, 1, 'A', 'ACE', 0, 0),
(42, 58, 1, 0, 3, 0, 15, 0, 1, 'A', 'ACE', 0, 0),
(42, 75, 0, 1, 0, 1, 30, 0, 1, 'A', 'FE', 0, 0),
(43, 58, 1, 0, 3, 0, 30, 0, 1, 'A', 'ACE', 0, 0),
(43, 75, 0, 1, 0, 1, 40, 0, 1, 'A', 'ACE', 0, 1),
(44, 58, 1, 0, 3, 0, 40, 0, 1, 'A', 'ACE', 0, 1),
(44, 75, 0, 1, 0, 1, 40, 15, 1, 'B', 'ACE', 0, 1),
(45, 58, 1, 0, 4, 0, 0, 0, 0, 'A', 'ACE', 0, 0),
(45, 75, 0, 1, 0, 1, 40, 30, 1, 'B', 'ACE', 0, 1),
(46, 58, 1, 0, 4, 0, 15, 0, 0, 'A', 'ACE', 0, 0),
(46, 75, 0, 1, 0, 1, 40, 40, 1, 'B', 'WS', 0, 0),
(47, 58, 1, 0, 4, 0, 30, 0, 0, 'A', 'ACE', 0, 0),
(47, 75, 0, 1, 0, 2, 0, 0, 0, 'B', 'ACE', 0, 0),
(48, 58, 1, 0, 4, 0, 40, 0, 0, 'A', 'ACE', 0, 0),
(48, 75, 0, 1, 0, 2, 0, 15, 0, 'B', 'ACE', 0, 0),
(49, 58, 1, 0, 5, 0, 0, 0, 1, 'A', 'ACE', 0, 0),
(49, 75, 0, 1, 0, 2, 0, 30, 0, 'B', 'WS', 0, 0),
(50, 58, 1, 0, 5, 0, 0, 15, 1, 'B', 'ACE', 0, 0),
(50, 75, 0, 1, 0, 2, 0, 40, 0, 'B', 'ACE', 0, 1),
(51, 58, 1, 0, 5, 0, 0, 30, 1, 'B', 'ACE', 0, 0),
(51, 75, 0, 1, 0, 3, 0, 0, 1, 'B', 'ACE', 0, 0),
(52, 58, 1, 0, 5, 0, 0, 40, 1, 'B', 'ACE', 0, 0),
(52, 75, 0, 1, 0, 3, 15, 0, 1, 'A', 'ACE', 0, 0),
(53, 58, 1, 0, 5, 1, 0, 0, 0, 'B', 'ACE', 0, 0),
(53, 75, 0, 1, 0, 3, 30, 0, 1, 'A', 'ACE', 0, 0),
(54, 58, 1, 0, 5, 1, 0, 15, 0, 'B', 'ACE', 0, 0),
(54, 75, 0, 1, 0, 3, 40, 0, 1, 'A', 'ACE', 0, 1),
(55, 58, 1, 0, 5, 1, 0, 30, 0, 'B', 'ACE', 0, 0),
(55, 75, 0, 1, 1, 3, 0, 0, 0, 'A', 'ACE', 0, 0),
(56, 58, 1, 0, 5, 1, 0, 40, 0, 'B', 'ACE', 0, 1),
(56, 75, 0, 1, 1, 3, 15, 0, 0, 'A', 'ACE', 0, 0),
(57, 58, 1, 0, 5, 1, 15, 40, 0, 'A', 'WS', 0, 1),
(57, 75, 0, 1, 1, 3, 30, 0, 0, 'A', 'ACE', 0, 0),
(58, 58, 1, 0, 5, 1, 30, 40, 0, 'A', 'ACE', 0, 1),
(58, 75, 0, 1, 1, 3, 40, 0, 0, 'A', 'ACE', 0, 0),
(59, 58, 1, 0, 5, 1, 40, 40, 0, 'A', 'ACE', 0, 0),
(59, 75, 0, 1, 2, 3, 0, 0, 1, 'A', 'ACE', 0, 0),
(60, 58, 1, 0, 5, 1, 4444, 40, 0, 'A', 'ACE', 0, 0),
(60, 75, 0, 1, 2, 3, 0, 15, 1, 'B', 'ACE', 0, 0),
(61, 75, 0, 1, 2, 3, 0, 30, 1, 'B', 'ACE', 0, 0),
(62, 75, 0, 1, 2, 3, 0, 40, 1, 'B', 'ACE', 0, 0),
(63, 75, 0, 1, 2, 4, 0, 0, 0, 'B', 'ACE', 0, 0),
(64, 75, 0, 1, 2, 4, 0, 15, 0, 'B', 'ACE', 0, 0),
(65, 75, 0, 1, 2, 4, 0, 30, 0, 'B', 'ACE', 0, 0),
(66, 75, 0, 1, 2, 4, 0, 40, 0, 'B', 'ACE', 0, 1),
(67, 75, 0, 1, 2, 5, 0, 0, 1, 'B', 'ACE', 0, 0),
(68, 75, 0, 1, 2, 5, 0, 15, 1, 'B', 'ACE', 0, 0),
(69, 75, 0, 1, 2, 5, 0, 30, 1, 'B', 'ACE', 0, 0),
(70, 75, 0, 1, 2, 5, 0, 40, 1, 'B', 'ACE', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Matchs`
--

CREATE TABLE IF NOT EXISTS `Matchs` (
  `IdMatch` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero du match',
  `IdHugeMatch` int(11) NOT NULL,
  `Category` enum('SM','SW','DM','DW','DX') NOT NULL COMMENT 'Specifie le type du match (Simple homme/femme, Double homme/femme, Double mixte) ',
  `IdOneOrTwo1` int(11) NOT NULL,
  `IdOneOrTwo2` int(11) NOT NULL,
  `Court` int(11) DEFAULT NULL COMMENT 'Le court de tennis',
  `DateStart` datetime DEFAULT NULL,
  `DateEnd` datetime DEFAULT NULL,
  `Statut` enum('','FINISH','SOON','PROGRESS','CANCEL') NOT NULL COMMENT 'Statut du match',
  `Winner` enum('A','B') DEFAULT NULL,
  PRIMARY KEY (`IdMatch`,`IdHugeMatch`,`IdOneOrTwo1`,`IdOneOrTwo2`),
  UNIQUE KEY `IdMatch` (`IdMatch`),
  KEY `IdOneOrTwo1` (`IdOneOrTwo1`),
  KEY `IdOneOrTwo2` (`IdOneOrTwo2`),
  KEY `IdHugeMatch` (`IdHugeMatch`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Contenu de la table `Matchs`
--

INSERT INTO `Matchs` (`IdMatch`, `IdHugeMatch`, `Category`, `IdOneOrTwo1`, `IdOneOrTwo2`, `Court`, `DateStart`, `DateEnd`, `Statut`, `Winner`) VALUES
(1, 1, 'SM', 1, 2, 2, '2015-03-23 12:27:16', '2015-03-23 05:00:00', 'FINISH', 'A'),
(2, 1, 'SM', 3, 4, 3, '2015-03-23 12:25:36', '2015-03-23 00:00:00', 'FINISH', 'A'),
(3, 1, 'SW', 5, 6, 2, '2015-03-23 12:46:58', '2015-03-23 12:47:18', 'FINISH', 'A'),
(4, 1, 'SW', 7, 8, 2, '2015-03-23 18:56:20', '2015-03-24 00:00:00', 'FINISH', 'B'),
(5, 1, 'DM', 97, 98, 1, '2015-03-23 18:20:59', '2015-03-24 00:00:00', 'FINISH', 'A'),
(6, 1, 'DW', 99, 100, 4, '2015-03-23 18:21:50', '2015-03-24 00:00:00', 'FINISH', 'B'),
(7, 1, 'DX', 101, 102, 7, '2015-03-23 19:29:04', '2015-03-24 00:00:00', 'FINISH', 'B'),
(8, 2, 'SM', 9, 10, 5, '2015-03-23 18:23:39', '2015-03-24 00:00:00', 'FINISH', 'A'),
(9, 2, 'SM', 11, 12, 6, '2015-03-23 20:02:07', '2015-03-23 20:03:20', 'FINISH', 'A'),
(10, 2, 'SW', 13, 14, 6, '2015-03-23 20:04:52', '2015-03-24 00:00:00', 'FINISH', 'B'),
(11, 2, 'SW', 15, 16, 7, '2015-03-23 20:20:44', '2015-03-23 20:21:42', 'FINISH', 'B'),
(12, 2, 'DM', 103, 104, 1, '2015-03-25 11:23:12', '2015-03-25 11:23:31', 'FINISH', 'A'),
(13, 2, 'DW', 105, 106, 7, '2015-03-23 20:12:44', '2015-03-23 20:15:18', 'FINISH', 'A'),
(14, 2, 'DX', 107, 108, 2, '2015-03-23 19:35:14', '2015-03-23 20:00:00', 'FINISH', 'A'),
(15, 3, 'SM', 17, 18, 3, '2015-03-25 11:23:52', '2015-03-25 11:24:01', 'FINISH', 'A'),
(16, 3, 'SM', 19, 20, 7, '2015-03-23 20:47:21', '2015-03-23 21:05:54', 'FINISH', 'A'),
(17, 3, 'SW', 21, 22, 6, '2015-03-25 11:24:41', '2015-03-25 11:25:03', 'FINISH', 'B'),
(18, 3, 'SW', 23, 24, 2, '2015-03-25 11:24:14', '2015-03-25 11:24:23', 'FINISH', 'B'),
(19, 3, 'DM', 109, 110, 2, '2015-03-25 11:25:22', '2015-03-25 11:25:39', 'FINISH', 'B'),
(20, 3, 'DW', 111, 112, 2, '2015-03-25 11:26:25', '2015-03-25 11:26:38', 'FINISH', 'A'),
(21, 3, 'DX', 113, 114, 3, '2015-03-25 11:27:10', '2015-03-25 11:27:22', 'FINISH', 'A'),
(22, 4, 'SM', 25, 26, 1, '2015-03-25 11:28:02', '2015-03-25 11:28:13', 'FINISH', 'A'),
(23, 4, 'SM', 27, 28, 3, '2015-03-25 11:29:01', '2015-03-25 11:29:16', 'FINISH', 'B'),
(24, 4, 'SW', 29, 30, 3, '2015-03-25 11:28:34', '2015-03-25 11:28:44', 'FINISH', 'A'),
(25, 4, 'SW', 31, 32, 3, '2015-03-25 11:30:03', '2015-03-25 11:30:13', 'FINISH', 'A'),
(26, 4, 'DM', 115, 116, 3, '2015-03-25 11:29:34', '2015-03-25 11:29:42', 'FINISH', 'A'),
(27, 4, 'DW', 117, 118, NULL, NULL, NULL, 'CANCEL', NULL),
(28, 4, 'DX', 119, 120, NULL, NULL, NULL, 'CANCEL', NULL),
(29, 5, 'SM', 33, 34, 2, '2015-03-25 11:43:11', '2015-03-25 11:43:22', 'FINISH', 'A'),
(30, 5, 'SM', 35, 36, 2, '2015-03-25 11:43:37', '2015-03-25 11:43:47', 'FINISH', 'A'),
(31, 5, 'SW', 37, 38, 2, '2015-03-25 11:44:03', '2015-03-25 11:44:12', 'FINISH', 'A'),
(32, 5, 'SW', 39, 40, 2, '2015-03-25 11:46:46', '2015-03-25 11:46:55', 'FINISH', 'A'),
(33, 5, 'DM', 121, 122, 3, '2015-03-25 11:48:05', '2015-03-25 11:49:13', 'FINISH', 'A'),
(34, 5, 'DW', 123, 124, 3, '2015-03-25 11:49:38', '2015-03-25 11:49:47', 'FINISH', 'A'),
(35, 5, 'DX', 125, 126, 3, '2015-03-25 11:50:58', '2015-03-25 11:51:21', 'FINISH', 'A'),
(36, 6, 'SM', 41, 42, 3, '2015-03-25 11:51:36', '2015-03-25 11:51:44', 'FINISH', 'A'),
(37, 6, 'SM', 43, 44, 3, '2015-03-25 11:53:56', '2015-03-25 11:54:12', 'FINISH', 'A'),
(38, 6, 'SW', 45, 46, 3, '2015-03-25 11:52:22', '2015-03-25 11:52:31', 'FINISH', 'A'),
(39, 6, 'SW', 47, 48, NULL, NULL, NULL, 'CANCEL', NULL),
(40, 6, 'DM', 127, 128, 3, '2015-03-25 11:55:36', '2015-03-25 11:56:03', 'FINISH', 'A'),
(41, 6, 'DW', 129, 130, 3, '2015-03-25 11:54:34', '2015-03-25 11:54:48', 'FINISH', 'A'),
(42, 6, 'DX', 131, 132, 2, '2015-03-25 11:56:23', '2015-03-25 11:56:40', 'FINISH', 'B'),
(43, 7, 'SM', 49, 50, 3, '2015-03-25 11:57:08', '2015-03-25 11:57:25', 'FINISH', 'A'),
(44, 7, 'SM', 51, 52, NULL, NULL, NULL, 'CANCEL', NULL),
(45, 7, 'SW', 53, 54, NULL, NULL, NULL, 'CANCEL', NULL),
(46, 7, 'SW', 55, 56, NULL, NULL, NULL, 'CANCEL', NULL),
(47, 7, 'DM', 133, 134, NULL, NULL, NULL, 'CANCEL', NULL),
(48, 7, 'DW', 135, 136, NULL, NULL, NULL, 'CANCEL', NULL),
(49, 7, 'DX', 137, 138, NULL, NULL, NULL, 'CANCEL', NULL),
(50, 8, 'SM', 57, 58, 3, '2015-03-25 11:59:43', '2015-03-25 11:59:54', 'FINISH', 'A'),
(51, 8, 'SM', 59, 60, 3, '2015-03-25 12:00:05', '2015-03-25 12:00:15', 'FINISH', 'A'),
(52, 8, 'SW', 61, 62, 6, '2015-03-25 12:00:50', '2015-03-25 12:00:59', 'FINISH', 'A'),
(53, 8, 'SW', 63, 64, 2, '2015-03-25 12:00:28', '2015-03-25 12:00:37', 'FINISH', 'A'),
(54, 8, 'DM', 139, 140, 3, '2015-03-25 12:01:19', '2015-03-25 12:01:43', 'FINISH', 'A'),
(55, 8, 'DW', 141, 142, 3, '2015-03-25 12:02:00', '2015-03-25 12:02:37', 'FINISH', 'A'),
(56, 8, 'DX', 143, 144, 3, '2015-03-25 12:03:11', '2015-03-25 12:03:21', 'FINISH', 'A'),
(57, 9, 'SM', 65, 66, 2, '2015-03-25 14:10:33', '2015-03-25 14:11:08', 'FINISH', 'A'),
(58, 9, 'SM', 67, 68, 1, '2015-03-25 09:17:21', '2015-03-25 15:00:00', 'FINISH', 'A'),
(59, 9, 'SW', 69, 70, NULL, NULL, NULL, 'SOON', NULL),
(60, 9, 'SW', 71, 72, NULL, NULL, NULL, 'SOON', NULL),
(61, 9, 'DM', 145, 146, NULL, NULL, NULL, 'SOON', NULL),
(62, 9, 'DW', 147, 148, 2, '2015-03-25 14:34:22', '2015-03-25 14:53:32', 'FINISH', 'A'),
(63, 9, 'DX', 149, 150, 2, '2015-03-25 14:15:39', '2015-03-25 15:00:00', 'FINISH', 'A'),
(64, 10, 'SM', 73, 74, NULL, NULL, NULL, 'SOON', NULL),
(65, 10, 'SM', 75, 76, NULL, NULL, NULL, 'SOON', NULL),
(66, 10, 'SW', 77, 78, NULL, NULL, NULL, 'SOON', NULL),
(67, 10, 'SW', 79, 80, NULL, NULL, NULL, 'SOON', NULL),
(68, 10, 'DM', 151, 152, NULL, NULL, NULL, 'SOON', NULL),
(69, 10, 'DW', 153, 154, NULL, NULL, NULL, 'SOON', NULL),
(70, 10, 'DX', 155, 156, NULL, NULL, NULL, 'SOON', NULL),
(71, 11, 'SM', 81, 82, NULL, NULL, NULL, 'SOON', NULL),
(72, 11, 'SM', 83, 84, NULL, NULL, NULL, 'SOON', NULL),
(73, 11, 'SW', 85, 86, NULL, NULL, NULL, 'SOON', NULL),
(74, 11, 'SW', 87, 88, NULL, NULL, NULL, 'SOON', NULL),
(75, 11, 'DM', 157, 158, 1, '2015-03-25 14:54:34', '2015-03-25 15:11:10', 'FINISH', 'B'),
(76, 11, 'DW', 159, 160, NULL, NULL, NULL, 'SOON', NULL),
(77, 11, 'DX', 161, 162, NULL, NULL, NULL, 'SOON', NULL),
(78, 12, 'SM', 89, 90, 7, '2015-03-25 15:17:44', NULL, 'PROGRESS', NULL),
(79, 12, 'SM', 91, 92, NULL, NULL, NULL, 'SOON', NULL),
(80, 12, 'SW', 93, 94, NULL, NULL, NULL, 'SOON', NULL),
(81, 12, 'SW', 95, 96, NULL, NULL, NULL, 'SOON', NULL),
(82, 12, 'DM', 163, 164, NULL, NULL, NULL, 'SOON', NULL),
(83, 12, 'DW', 165, 166, NULL, NULL, NULL, 'SOON', NULL),
(84, 12, 'DX', 167, 168, NULL, NULL, NULL, 'SOON', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `OneOrTwo`
--

CREATE TABLE IF NOT EXISTS `OneOrTwo` (
  `IdOneOrTwo` int(4) NOT NULL AUTO_INCREMENT,
  `IdPlayer_A` int(11) DEFAULT NULL,
  `IdPlayer_B` int(11) DEFAULT NULL,
  `Category` enum('','Simple','Double') NOT NULL,
  PRIMARY KEY (`IdOneOrTwo`),
  UNIQUE KEY `IdOneOrTwo` (`IdOneOrTwo`),
  KEY `IdPlayer_A` (`IdPlayer_A`),
  KEY `IdPlayer_B` (`IdPlayer_B`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=169 ;

--
-- Contenu de la table `OneOrTwo`
--

INSERT INTO `OneOrTwo` (`IdOneOrTwo`, `IdPlayer_A`, `IdPlayer_B`, `Category`) VALUES
(1, 2, NULL, 'Simple'),
(2, 11, NULL, 'Simple'),
(3, 2, NULL, 'Simple'),
(4, 12, NULL, 'Simple'),
(5, 6, NULL, 'Simple'),
(6, 8, NULL, 'Simple'),
(7, 4, NULL, 'Simple'),
(8, 7, NULL, 'Simple'),
(9, 22, NULL, 'Simple'),
(10, 43, NULL, 'Simple'),
(11, 23, NULL, 'Simple'),
(12, 44, NULL, 'Simple'),
(13, 20, NULL, 'Simple'),
(14, 46, NULL, 'Simple'),
(15, 19, NULL, 'Simple'),
(16, 46, NULL, 'Simple'),
(17, 25, NULL, 'Simple'),
(18, 32, NULL, 'Simple'),
(19, 25, NULL, 'Simple'),
(20, 32, NULL, 'Simple'),
(21, 26, NULL, 'Simple'),
(22, 34, NULL, 'Simple'),
(23, 28, NULL, 'Simple'),
(24, 34, NULL, 'Simple'),
(25, 39, NULL, 'Simple'),
(26, 15, NULL, 'Simple'),
(27, 39, NULL, 'Simple'),
(28, 15, NULL, 'Simple'),
(29, 40, NULL, 'Simple'),
(30, 13, NULL, 'Simple'),
(31, 37, NULL, 'Simple'),
(32, 13, NULL, 'Simple'),
(33, 2, NULL, 'Simple'),
(34, 23, NULL, 'Simple'),
(35, 2, NULL, 'Simple'),
(36, 23, NULL, 'Simple'),
(37, 4, NULL, 'Simple'),
(38, 19, NULL, 'Simple'),
(39, 1, NULL, 'Simple'),
(40, 20, NULL, 'Simple'),
(41, 25, NULL, 'Simple'),
(42, 39, NULL, 'Simple'),
(43, 25, NULL, 'Simple'),
(44, 38, NULL, 'Simple'),
(45, 26, NULL, 'Simple'),
(46, 40, NULL, 'Simple'),
(47, NULL, NULL, 'Simple'),
(48, NULL, NULL, 'Simple'),
(49, 10, NULL, 'Simple'),
(50, 44, NULL, 'Simple'),
(51, NULL, NULL, 'Simple'),
(52, NULL, NULL, 'Simple'),
(53, NULL, NULL, 'Simple'),
(54, NULL, NULL, 'Simple'),
(55, NULL, NULL, 'Simple'),
(56, NULL, NULL, 'Simple'),
(57, 32, NULL, 'Simple'),
(58, 15, NULL, 'Simple'),
(59, 32, NULL, 'Simple'),
(60, 15, NULL, 'Simple'),
(61, 33, NULL, 'Simple'),
(62, 13, NULL, 'Simple'),
(63, 34, NULL, 'Simple'),
(64, 13, NULL, 'Simple'),
(65, 2, NULL, 'Simple'),
(66, 25, NULL, 'Simple'),
(67, 2, NULL, 'Simple'),
(68, 27, NULL, 'Simple'),
(69, NULL, NULL, 'Simple'),
(70, NULL, NULL, 'Simple'),
(71, NULL, NULL, 'Simple'),
(72, NULL, NULL, 'Simple'),
(73, NULL, NULL, 'Simple'),
(74, NULL, NULL, 'Simple'),
(75, NULL, NULL, 'Simple'),
(76, NULL, NULL, 'Simple'),
(77, NULL, NULL, 'Simple'),
(78, NULL, NULL, 'Simple'),
(79, NULL, NULL, 'Simple'),
(80, NULL, NULL, 'Simple'),
(81, NULL, NULL, 'Simple'),
(82, NULL, NULL, 'Simple'),
(83, NULL, NULL, 'Simple'),
(84, NULL, NULL, 'Simple'),
(85, NULL, NULL, 'Simple'),
(86, NULL, NULL, 'Simple'),
(87, NULL, NULL, 'Simple'),
(88, NULL, NULL, 'Simple'),
(89, 43, NULL, 'Simple'),
(90, 16, NULL, 'Simple'),
(91, NULL, NULL, 'Simple'),
(92, NULL, NULL, 'Simple'),
(93, NULL, NULL, 'Simple'),
(94, NULL, NULL, 'Simple'),
(95, NULL, NULL, 'Simple'),
(96, NULL, NULL, 'Simple'),
(97, 1, 2, 'Double'),
(98, 10, 11, 'Double'),
(99, 4, 5, 'Double'),
(100, 7, 8, 'Double'),
(101, 2, 4, 'Double'),
(102, 10, 9, 'Double'),
(103, 22, 23, 'Double'),
(104, 43, 44, 'Double'),
(105, 21, 20, 'Double'),
(106, 47, 48, 'Double'),
(107, 21, 22, 'Double'),
(108, 44, 46, 'Double'),
(109, 25, 27, 'Double'),
(110, 32, 31, 'Double'),
(111, 26, 28, 'Double'),
(112, 34, 33, 'Double'),
(113, 27, 28, 'Double'),
(114, 31, 33, 'Double'),
(115, 38, 39, 'Double'),
(116, 15, 14, 'Double'),
(117, NULL, NULL, 'Double'),
(118, NULL, NULL, 'Double'),
(119, NULL, NULL, 'Double'),
(120, NULL, NULL, 'Double'),
(121, 2, 3, 'Double'),
(122, 23, 22, 'Double'),
(123, 1, 4, 'Double'),
(124, 20, 19, 'Double'),
(125, 2, 4, 'Double'),
(126, 19, 22, 'Double'),
(127, 25, 27, 'Double'),
(128, 39, 38, 'Double'),
(129, 26, 28, 'Double'),
(130, 40, 37, 'Double'),
(131, 26, 25, 'Double'),
(132, 38, 37, 'Double'),
(133, NULL, NULL, 'Double'),
(134, NULL, NULL, 'Double'),
(135, NULL, NULL, 'Double'),
(136, NULL, NULL, 'Double'),
(137, NULL, NULL, 'Double'),
(138, NULL, NULL, 'Double'),
(139, 31, 32, 'Double'),
(140, 15, 14, 'Double'),
(141, 33, 34, 'Double'),
(142, 13, 17, 'Double'),
(143, 32, 33, 'Double'),
(144, 13, 14, 'Double'),
(145, NULL, NULL, 'Double'),
(146, NULL, NULL, 'Double'),
(147, 4, 1, 'Double'),
(148, 28, 26, 'Double'),
(149, 1, 2, 'Double'),
(150, 26, 25, 'Double'),
(151, NULL, NULL, 'Double'),
(152, NULL, NULL, 'Double'),
(153, NULL, NULL, 'Double'),
(154, NULL, NULL, 'Double'),
(155, NULL, NULL, 'Double'),
(156, NULL, NULL, 'Double'),
(157, 12, 11, 'Double'),
(158, 32, 31, 'Double'),
(159, NULL, NULL, 'Double'),
(160, NULL, NULL, 'Double'),
(161, NULL, NULL, 'Double'),
(162, NULL, NULL, 'Double'),
(163, NULL, NULL, 'Double'),
(164, NULL, NULL, 'Double'),
(165, NULL, NULL, 'Double'),
(166, NULL, NULL, 'Double'),
(167, NULL, NULL, 'Double'),
(168, NULL, NULL, 'Double');

-- --------------------------------------------------------

--
-- Structure de la table `Players`
--

CREATE TABLE IF NOT EXISTS `Players` (
  `IdPlayer` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Numero unique attribué a un Player ',
  `Team` varchar(20) NOT NULL,
  `Name` varchar(30) NOT NULL COMMENT 'Name de famille des Players',
  `FirstName` varchar(30) NOT NULL COMMENT 'FirstName des Players',
  `Sex` enum('M','W') NOT NULL COMMENT 'Le Sex du Player',
  `DateBirth` date DEFAULT NULL COMMENT 'Date de naissance',
  `Url` text,
  PRIMARY KEY (`IdPlayer`,`Team`),
  UNIQUE KEY `IdPlayer` (`IdPlayer`),
  KEY `Team` (`Team`),
  KEY `IdPlayer_2` (`IdPlayer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table enregistrement des Players' AUTO_INCREMENT=119 ;

--
-- Contenu de la table `Players`
--

INSERT INTO `Players` (`IdPlayer`, `Team`, `Name`, `FirstName`, `Sex`, `DateBirth`, `Url`) VALUES
(1, 'BEL', 'CANedit', 'Lou', 'W', '2015-02-28', NULL),
(2, 'BEL', 'STORME', 'James Junior', 'M', '1994-12-13', NULL),
(3, 'BEL', 'MERCKX', 'Jonas', 'M', '1993-10-04', NULL),
(4, 'BEL', 'DELEFORTRIE', 'Anouk', 'W', '1990-11-29', NULL),
(5, 'BEL', 'DE SUTTER', 'Justine', 'W', '1995-03-20', NULL),
(6, 'BEL', 'DOM', 'Evelyne', 'W', '0000-00-00', NULL),
(7, 'CHN', 'ZHU', 'Ai Wen', 'W', '1994-09-22', NULL),
(8, 'CHN', 'YANG', 'Yi', 'W', '1989-09-27', NULL),
(9, 'CHN', 'WANG', 'Yue', 'W', '1994-01-23', NULL),
(10, 'CHN', 'ZHONG', 'Su Hao', 'M', '1997-03-19', NULL),
(11, 'CHN', 'PENG', 'Changwei', 'M', '1994-02-16', NULL),
(12, 'CHN', 'CAI', 'Zhao', 'M', '1994-07-05', NULL),
(13, 'USA', 'VAN NGUYEN', 'Chanelle', 'W', '1994-01-19', NULL),
(14, 'USA', 'RUBIN', 'Noah', 'M', '1996-02-21', NULL),
(15, 'USA', 'PASHA', 'Teahan', 'M', '1992-07-15', NULL),
(16, 'USA', 'KWIATKOWSKI', 'Thai', 'M', '1995-02-13', NULL),
(17, 'USA', 'ELBABA', 'Julia', 'W', '1994-06-13', NULL),
(18, 'USA', 'ANDERSON', 'Robin', 'W', '1993-04-12', NULL),
(19, 'DEU', 'STEMMER', 'Stefanie', 'W', '1993-08-03', NULL),
(20, 'DEU', 'FUCHS', 'Anna­Benita', 'W', '1992-11-06', NULL),
(21, 'DEU', 'SCHELENZ', 'Desiree', 'W', '0000-00-00', NULL),
(22, 'DEU', 'REGUS', 'Ralph', 'M', '1990-07-18', NULL),
(23, 'DEU', 'WETZEL', 'Mattis', 'M', '1989-11-21', NULL),
(24, 'DEU', 'DORNBUSCH', 'Michel', 'M', '1990-09-30', NULL),
(25, 'GBR', 'WALSH', 'Darren', 'M', '1989-01-16', NULL),
(26, 'GBR', 'FITZPATRICK', 'Anna', 'W', '1989-04-06', NULL),
(27, 'GBR', 'EECKELAERS', 'Nicholas', 'M', '1993-12-11', NULL),
(28, 'GBR', 'WALKER', 'Alexandra', 'W', '1992-09-21', NULL),
(29, 'GBR', 'REN', 'Jessica', 'W', '1994-08-23', NULL),
(30, 'GBR', 'WHITHEHOUSE', 'Mark', 'M', '1993-05-01', NULL),
(31, 'IRL', 'REID', 'Cian', 'M', '1995-06-08', NULL),
(32, 'IRL', 'SHEEHAN', 'Phillip', 'M', '1996-01-15', NULL),
(33, 'IRL', 'BYRNE', 'Julie', 'W', '1997-07-16', NULL),
(34, 'IRL', 'BENJENARU', 'Carola', 'W', '1997-06-29', NULL),
(35, 'IRL', 'KENNEDY', 'Sinead', 'W', '1993-10-18', NULL),
(36, 'IRL', 'HUGHES', 'Jack', 'M', '1991-04-23', NULL),
(37, 'RUS', 'DANILINA', 'Anna', 'W', '1995-08-20', NULL),
(38, 'RUS', 'ELISTRATOV', 'Evgeny', 'M', '1994-12-28', NULL),
(39, 'RUS', 'KOZYUKOV', 'Vitaly', 'M', '1994-06-02', NULL),
(40, 'RUS', 'VALETOVA', 'Iuliia', 'W', '1994-07-18', NULL),
(41, 'RUS', 'KAMENSKAYA', 'Victoria', 'W', '1991-11-26', NULL),
(42, 'RUS', 'POLYAKOV', 'Vladimir', 'M', '1994-06-18', NULL),
(43, 'FRA', 'HOANG', 'Antoine', 'M', '1995-11-04', NULL),
(44, 'FRA', 'REBOUL', 'Fabien', 'M', '1995-09-09', NULL),
(45, 'FRA', 'JACQ', 'Grégoire', 'M', '1992-11-09', NULL),
(46, 'FRA', 'BACQUIE', 'Alice', 'W', '1995-07-23', NULL),
(47, 'FRA', 'GIRARD', 'Rachel', 'W', '1994-01-10', NULL),
(48, 'FRA', 'LARRIERE', 'Victoria', 'W', '1991-05-02', NULL),
(118, 'CHN', 'test', 'ok', 'M', '2015-03-25', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `StatMatch`
--

CREATE TABLE IF NOT EXISTS `StatMatch` (
  `IdMatch` int(11) NOT NULL,
  `Ref` enum('A','B') NOT NULL,
  `Category` enum('SIMPLE','DOUBLE') NOT NULL,
  `IdPlayer` int(11) NOT NULL,
  `ACE` int(11) NOT NULL,
  `WS` int(11) NOT NULL COMMENT 'Winner Serve',
  `WP` int(11) NOT NULL COMMENT 'Winner Point',
  `DF` int(11) NOT NULL COMMENT 'Double Fault',
  `UE` int(11) NOT NULL COMMENT 'Unforced Error',
  `FE` int(11) NOT NULL COMMENT 'Forced Error',
  `FSF` int(11) NOT NULL COMMENT 'First Serve Fault',
  `Break` int(11) NOT NULL,
  PRIMARY KEY (`IdMatch`,`IdPlayer`),
  KEY `IdPlayer` (`IdPlayer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `StatMatch`
--

INSERT INTO `StatMatch` (`IdMatch`, `Ref`, `Category`, `IdPlayer`, `ACE`, `WS`, `WP`, `DF`, `UE`, `FE`, `FSF`, `Break`) VALUES
(1, 'A', 'SIMPLE', 2, 0, 0, 0, 0, 0, 0, 0, 0),
(1, 'B', 'SIMPLE', 11, 0, 0, 0, 0, 0, 2, 0, 0),
(2, 'A', 'SIMPLE', 2, 1, 0, 0, 0, 0, 1, 1, 0),
(2, 'B', 'SIMPLE', 12, 0, 0, 0, 0, 0, 2, 0, 0),
(3, 'A', 'SIMPLE', 6, 2, 0, 0, 0, 1, 2, 1, 1),
(3, 'B', 'SIMPLE', 8, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'A', 'SIMPLE', 4, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'B', 'SIMPLE', 7, 0, 0, 0, 0, 0, 2, 0, 0),
(5, 'A', 'DOUBLE', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'A', 'DOUBLE', 2, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'B', 'DOUBLE', 10, 0, 0, 0, 0, 0, 4, 0, 1),
(5, 'B', 'DOUBLE', 11, 0, 0, 0, 0, 0, 4, 0, 1),
(6, 'A', 'DOUBLE', 4, 0, 0, 0, 0, 0, 4, 0, 0),
(6, 'A', 'DOUBLE', 5, 0, 0, 0, 0, 0, 4, 0, 0),
(6, 'B', 'DOUBLE', 7, 0, 0, 0, 0, 0, 1, 0, 0),
(6, 'B', 'DOUBLE', 8, 0, 0, 0, 0, 0, 1, 0, 0),
(7, 'A', 'DOUBLE', 2, 0, 0, 0, 0, 0, 1, 0, 0),
(7, 'A', 'DOUBLE', 4, 0, 0, 0, 0, 0, 1, 0, 0),
(7, 'B', 'DOUBLE', 9, 0, 0, 0, 0, 0, 1, 0, 0),
(7, 'B', 'DOUBLE', 10, 0, 0, 0, 0, 0, 1, 0, 0),
(8, 'A', 'SIMPLE', 22, 1, 0, 0, 0, 0, 0, 0, 0),
(8, 'B', 'SIMPLE', 43, 3, 1, 0, 0, 0, 0, 0, 1),
(9, 'A', 'SIMPLE', 23, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 'B', 'SIMPLE', 44, 5, 0, 0, 0, 0, 4, 0, 1),
(10, 'A', 'SIMPLE', 20, 0, 0, 0, 0, 0, 3, 0, 2),
(10, 'B', 'SIMPLE', 46, 3, 0, 0, 0, 0, 5, 0, 2),
(11, 'A', 'SIMPLE', 19, 0, 0, 0, 0, 0, 1, 0, 0),
(11, 'B', 'SIMPLE', 46, 0, 0, 0, 0, 0, 2, 0, 0),
(12, 'A', 'DOUBLE', 22, 1, 0, 0, 0, 1, 2, 0, 0),
(12, 'A', 'DOUBLE', 23, 1, 0, 0, 0, 1, 2, 0, 0),
(12, 'B', 'DOUBLE', 43, 1, 0, 0, 0, 1, 1, 0, 0),
(12, 'B', 'DOUBLE', 44, 1, 0, 0, 0, 1, 1, 0, 0),
(13, 'A', 'DOUBLE', 20, 8, 0, 0, 0, 0, 4, 0, 2),
(13, 'A', 'DOUBLE', 21, 8, 0, 0, 0, 0, 4, 0, 2),
(13, 'B', 'DOUBLE', 47, 0, 0, 0, 0, 0, 15, 0, 2),
(13, 'B', 'DOUBLE', 48, 0, 0, 0, 0, 0, 15, 0, 2),
(14, 'A', 'DOUBLE', 21, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 'A', 'DOUBLE', 22, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 'B', 'DOUBLE', 44, 0, 0, 0, 0, 0, 4, 0, 1),
(14, 'B', 'DOUBLE', 46, 0, 0, 0, 0, 0, 4, 0, 1),
(15, 'A', 'SIMPLE', 25, 1, 0, 0, 0, 0, 0, 0, 0),
(15, 'B', 'SIMPLE', 32, 0, 0, 0, 0, 1, 1, 0, 0),
(16, 'A', 'SIMPLE', 25, 0, 0, 0, 0, 0, 1, 0, 0),
(16, 'B', 'SIMPLE', 32, 0, 0, 0, 0, 0, 1, 0, 0),
(17, 'A', 'SIMPLE', 26, 3, 0, 1, 0, 0, 0, 0, 0),
(17, 'B', 'SIMPLE', 34, 1, 0, 2, 0, 1, 2, 0, 3),
(18, 'A', 'SIMPLE', 28, 1, 0, 0, 0, 0, 0, 0, 0),
(18, 'B', 'SIMPLE', 34, 0, 0, 0, 0, 1, 0, 0, 0),
(19, 'A', 'DOUBLE', 25, 3, 0, 0, 0, 1, 2, 0, 0),
(19, 'A', 'DOUBLE', 27, 3, 0, 0, 0, 1, 2, 0, 0),
(19, 'B', 'DOUBLE', 31, 0, 0, 0, 0, 2, 1, 0, 0),
(19, 'B', 'DOUBLE', 32, 0, 0, 0, 0, 2, 1, 0, 0),
(20, 'A', 'DOUBLE', 26, 1, 0, 0, 0, 1, 0, 0, 0),
(20, 'A', 'DOUBLE', 28, 1, 0, 0, 0, 1, 0, 0, 0),
(20, 'B', 'DOUBLE', 33, 0, 0, 1, 0, 0, 1, 0, 0),
(20, 'B', 'DOUBLE', 34, 0, 0, 1, 0, 0, 1, 0, 0),
(21, 'A', 'DOUBLE', 27, 2, 0, 1, 0, 0, 0, 0, 0),
(21, 'A', 'DOUBLE', 28, 2, 0, 1, 0, 0, 0, 0, 0),
(21, 'B', 'DOUBLE', 31, 0, 0, 0, 0, 1, 1, 0, 0),
(21, 'B', 'DOUBLE', 33, 0, 0, 0, 0, 1, 1, 0, 0),
(22, 'B', 'SIMPLE', 15, 0, 0, 0, 0, 1, 1, 0, 0),
(22, 'A', 'SIMPLE', 39, 1, 0, 1, 0, 0, 0, 0, 0),
(23, 'B', 'SIMPLE', 15, 1, 0, 0, 0, 0, 1, 0, 0),
(23, 'A', 'SIMPLE', 39, 0, 0, 0, 0, 3, 1, 0, 0),
(24, 'B', 'SIMPLE', 13, 0, 0, 0, 0, 1, 1, 0, 0),
(24, 'A', 'SIMPLE', 40, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 'B', 'SIMPLE', 13, 1, 0, 0, 0, 0, 1, 0, 0),
(25, 'A', 'SIMPLE', 37, 0, 0, 0, 0, 1, 0, 0, 0),
(26, 'B', 'DOUBLE', 14, 0, 0, 0, 0, 0, 1, 0, 0),
(26, 'B', 'DOUBLE', 15, 0, 0, 0, 0, 0, 1, 0, 0),
(26, 'A', 'DOUBLE', 38, 0, 0, 0, 0, 1, 0, 0, 0),
(26, 'A', 'DOUBLE', 39, 0, 0, 0, 0, 1, 0, 0, 0),
(29, 'A', 'SIMPLE', 2, 1, 0, 0, 0, 0, 0, 0, 0),
(29, 'B', 'SIMPLE', 23, 0, 0, 1, 0, 0, 1, 0, 0),
(30, 'A', 'SIMPLE', 2, 2, 0, 0, 0, 1, 0, 0, 0),
(30, 'B', 'SIMPLE', 23, 1, 0, 1, 0, 0, 0, 0, 0),
(31, 'A', 'SIMPLE', 4, 1, 0, 0, 0, 1, 0, 0, 0),
(31, 'B', 'SIMPLE', 19, 0, 0, 0, 0, 0, 1, 0, 0),
(32, 'A', 'SIMPLE', 1, 0, 0, 0, 0, 1, 0, 0, 0),
(32, 'B', 'SIMPLE', 20, 1, 0, 0, 0, 0, 0, 0, 0),
(33, 'A', 'DOUBLE', 2, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 'A', 'DOUBLE', 3, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 'B', 'DOUBLE', 22, 0, 0, 0, 0, 1, 0, 0, 0),
(33, 'B', 'DOUBLE', 23, 0, 0, 0, 0, 1, 0, 0, 0),
(34, 'A', 'DOUBLE', 1, 1, 0, 0, 0, 1, 0, 0, 0),
(34, 'A', 'DOUBLE', 4, 1, 0, 0, 0, 1, 0, 0, 0),
(34, 'B', 'DOUBLE', 19, 0, 0, 0, 0, 1, 1, 0, 0),
(34, 'B', 'DOUBLE', 20, 0, 0, 0, 0, 1, 1, 0, 0),
(35, 'A', 'DOUBLE', 2, 0, 0, 0, 0, 1, 0, 0, 0),
(35, 'A', 'DOUBLE', 4, 0, 0, 0, 0, 1, 0, 0, 0),
(35, 'B', 'DOUBLE', 19, 0, 0, 0, 0, 1, 0, 0, 0),
(35, 'B', 'DOUBLE', 22, 0, 0, 0, 0, 1, 0, 0, 0),
(36, 'A', 'SIMPLE', 25, 1, 0, 0, 0, 0, 0, 0, 0),
(36, 'B', 'SIMPLE', 39, 1, 0, 1, 0, 0, 0, 0, 0),
(37, 'A', 'SIMPLE', 25, 1, 0, 0, 0, 0, 1, 0, 0),
(37, 'B', 'SIMPLE', 38, 1, 0, 0, 0, 0, 1, 0, 0),
(38, 'A', 'SIMPLE', 26, 0, 0, 0, 0, 1, 0, 0, 0),
(38, 'B', 'SIMPLE', 40, 0, 0, 0, 0, 0, 1, 0, 0),
(40, 'A', 'DOUBLE', 25, 1, 0, 0, 0, 0, 0, 0, 0),
(40, 'A', 'DOUBLE', 27, 1, 0, 0, 0, 0, 0, 0, 0),
(40, 'B', 'DOUBLE', 38, 1, 0, 0, 0, 0, 1, 0, 0),
(40, 'B', 'DOUBLE', 39, 1, 0, 0, 0, 0, 1, 0, 0),
(41, 'A', 'DOUBLE', 26, 1, 0, 0, 0, 1, 0, 0, 0),
(41, 'A', 'DOUBLE', 28, 1, 0, 0, 0, 1, 0, 0, 0),
(41, 'B', 'DOUBLE', 37, 0, 0, 0, 0, 1, 1, 0, 0),
(41, 'B', 'DOUBLE', 40, 0, 0, 0, 0, 1, 1, 0, 0),
(42, 'A', 'DOUBLE', 25, 1, 0, 0, 0, 1, 1, 0, 0),
(42, 'A', 'DOUBLE', 26, 1, 0, 0, 0, 1, 1, 0, 0),
(42, 'B', 'DOUBLE', 37, 1, 0, 0, 0, 1, 1, 0, 0),
(42, 'B', 'DOUBLE', 38, 1, 0, 0, 0, 1, 1, 0, 0),
(43, 'A', 'SIMPLE', 10, 1, 0, 0, 0, 0, 0, 0, 0),
(43, 'B', 'SIMPLE', 44, 0, 0, 0, 0, 0, 1, 0, 0),
(50, 'B', 'SIMPLE', 15, 0, 0, 0, 0, 1, 1, 0, 0),
(50, 'A', 'SIMPLE', 32, 1, 0, 0, 0, 1, 0, 0, 0),
(51, 'B', 'SIMPLE', 15, 1, 0, 0, 0, 0, 1, 0, 0),
(51, 'A', 'SIMPLE', 32, 1, 0, 0, 0, 0, 0, 0, 0),
(52, 'B', 'SIMPLE', 13, 1, 0, 0, 0, 0, 1, 0, 0),
(52, 'A', 'SIMPLE', 33, 1, 0, 0, 0, 0, 0, 0, 0),
(53, 'B', 'SIMPLE', 13, 1, 0, 0, 0, 0, 1, 0, 0),
(53, 'A', 'SIMPLE', 34, 0, 0, 0, 0, 0, 0, 0, 0),
(54, 'B', 'DOUBLE', 14, 1, 0, 0, 0, 0, 1, 0, 0),
(54, 'B', 'DOUBLE', 15, 1, 0, 0, 0, 0, 1, 0, 0),
(54, 'A', 'DOUBLE', 31, 1, 0, 0, 0, 1, 0, 0, 0),
(54, 'A', 'DOUBLE', 32, 1, 0, 0, 0, 1, 0, 0, 0),
(55, 'B', 'DOUBLE', 13, 1, 0, 0, 0, 0, 0, 0, 0),
(55, 'B', 'DOUBLE', 17, 1, 0, 0, 0, 0, 0, 0, 0),
(55, 'A', 'DOUBLE', 33, 1, 0, 0, 0, 0, 0, 0, 0),
(55, 'A', 'DOUBLE', 34, 1, 0, 0, 0, 0, 0, 0, 0),
(56, 'B', 'DOUBLE', 13, 1, 0, 0, 0, 0, 0, 0, 0),
(56, 'B', 'DOUBLE', 14, 1, 0, 0, 0, 0, 0, 0, 0),
(56, 'A', 'DOUBLE', 32, 1, 0, 0, 0, 0, 0, 0, 0),
(56, 'A', 'DOUBLE', 33, 1, 0, 0, 0, 0, 0, 0, 0),
(57, 'A', 'SIMPLE', 2, 0, 0, 0, 0, 1, 1, 0, 0),
(57, 'B', 'SIMPLE', 25, 0, 0, 0, 0, 1, 1, 0, 0),
(58, 'A', 'SIMPLE', 2, 45, 4, 0, 0, 0, 0, 0, 7),
(58, 'B', 'SIMPLE', 27, 10, 0, 1, 0, 0, 0, 0, 2),
(62, 'A', 'DOUBLE', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(62, 'A', 'DOUBLE', 4, 0, 0, 0, 0, 0, 0, 0, 0),
(62, 'B', 'DOUBLE', 26, 0, 0, 0, 0, 0, 0, 0, 0),
(62, 'B', 'DOUBLE', 28, 0, 0, 0, 0, 0, 0, 0, 0),
(63, 'A', 'DOUBLE', 1, 0, 0, 0, 0, 0, 0, 0, 0),
(63, 'A', 'DOUBLE', 2, 0, 0, 0, 0, 0, 0, 0, 0),
(63, 'B', 'DOUBLE', 25, 0, 0, 0, 0, 0, 0, 0, 0),
(63, 'B', 'DOUBLE', 26, 0, 0, 0, 0, 0, 0, 0, 0),
(75, 'A', 'DOUBLE', 11, 15, 0, 0, 0, 0, 6, 0, 3),
(75, 'A', 'DOUBLE', 12, 15, 0, 0, 0, 0, 6, 0, 3),
(75, 'B', 'DOUBLE', 31, 39, 4, 0, 0, 0, 6, 0, 8),
(75, 'B', 'DOUBLE', 32, 39, 4, 0, 0, 0, 6, 0, 8),
(78, 'B', 'SIMPLE', 16, 0, 0, 0, 0, 0, 0, 0, 0),
(78, 'A', 'SIMPLE', 43, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `Teams`
--

CREATE TABLE IF NOT EXISTS `Teams` (
  `NameTeam` varchar(20) NOT NULL DEFAULT '',
  `IdTeam` int(11) NOT NULL,
  `CompleteName` varchar(50) DEFAULT NULL,
  `Points` int(20) DEFAULT '0',
  `URL` text,
  PRIMARY KEY (`NameTeam`,`IdTeam`),
  UNIQUE KEY `NameTeam` (`NameTeam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Teams`
--

INSERT INTO `Teams` (`NameTeam`, `IdTeam`, `CompleteName`, `Points`, `URL`) VALUES
('BEL', 3, 'Belgique', 15, NULL),
('CHN', 2, 'Chine', 4, NULL),
('DEU', 4, 'Allemagne', 5, NULL),
('FRA', 1, 'France', 2, NULL),
('GBR', 8, 'Angleterre ', 9, NULL),
('IRL', 7, 'Irlande', 11, NULL),
('RUS', 5, 'Russie', 5, NULL),
('USA', 6, 'Etat-Unis', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `Tournament`
--

CREATE TABLE IF NOT EXISTS `Tournament` (
  `Name` varchar(50) NOT NULL,
  `Stage` enum('','1/4','1/2','Final','SETTING-UP') NOT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Tournament`
--

INSERT INTO `Tournament` (`Name`, `Stage`) VALUES
('Master-U 2036', 'Final');

-- --------------------------------------------------------

--
-- Structure de la table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `Username` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Password` text CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  PRIMARY KEY (`Username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Users`
--

INSERT INTO `Users` (`Username`, `Password`) VALUES
('john', '88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589'),
('perceval', '21ebf8e646dbe76bf6365c4f0050f6235b3a59296a400fe0c1c5223c86b0908e');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `DisplayScores`
--
ALTER TABLE `DisplayScores`
  ADD CONSTRAINT `DisplayScores_ibfk_2` FOREIGN KEY (`IdOneOrTwo`) REFERENCES `OneOrTwo` (`IdOneOrTwo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `DisplayScores_ibfk_1` FOREIGN KEY (`IdMatch`) REFERENCES `Matchs` (`IdMatch`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `HugeMatch`
--
ALTER TABLE `HugeMatch`
  ADD CONSTRAINT `HugeMatch_ibfk_5` FOREIGN KEY (`Team_B`) REFERENCES `Teams` (`NameTeam`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `HugeMatch_ibfk_3` FOREIGN KEY (`IdMatch`) REFERENCES `Matchs` (`IdMatch`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `HugeMatch_ibfk_4` FOREIGN KEY (`Team_A`) REFERENCES `Teams` (`NameTeam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Log`
--
ALTER TABLE `Log`
  ADD CONSTRAINT `Log_ibfk_1` FOREIGN KEY (`IdMatch`) REFERENCES `Matchs` (`IdMatch`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Matchs`
--
ALTER TABLE `Matchs`
  ADD CONSTRAINT `Matchs_ibfk_2` FOREIGN KEY (`IdOneOrTwo2`) REFERENCES `OneOrTwo` (`IdOneOrTwo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Matchs_ibfk_1` FOREIGN KEY (`IdOneOrTwo1`) REFERENCES `OneOrTwo` (`IdOneOrTwo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Matchs_ibfk_3` FOREIGN KEY (`IdHugeMatch`) REFERENCES `HugeMatch` (`IdHugeMatch`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `OneOrTwo`
--
ALTER TABLE `OneOrTwo`
  ADD CONSTRAINT `OneOrTwo_ibfk_1` FOREIGN KEY (`IdPlayer_A`) REFERENCES `Players` (`IdPlayer`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `OneOrTwo_ibfk_2` FOREIGN KEY (`IdPlayer_B`) REFERENCES `Players` (`IdPlayer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Players`
--
ALTER TABLE `Players`
  ADD CONSTRAINT `Players_ibfk_1` FOREIGN KEY (`Team`) REFERENCES `Teams` (`NameTeam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `StatMatch`
--
ALTER TABLE `StatMatch`
  ADD CONSTRAINT `StatMatch_ibfk_1` FOREIGN KEY (`IdMatch`) REFERENCES `Matchs` (`IdMatch`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `StatMatch_ibfk_2` FOREIGN KEY (`IdPlayer`) REFERENCES `Players` (`IdPlayer`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
